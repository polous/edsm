# -*- encoding: utf-8-*-
from Toolkit.SocialMediaContentProviders.Twitter import TwitterContentProvider
from Toolkit.EventDetectionAlgorithms.TextualDBSCAN import TextualDBSCANEventDetectionAlgorithm
from Toolkit.DataFilters.RemoveSimilarsFromSameUser import RemoveSimilarsFromSameUser
from Toolkit.EventFilters.ContentClusterSizeThreshold import ContentClusterSizeThreshold
from Toolkit.EventFilters.ContributorNumberThreshold import ContributorNumberThreshold
from Toolkit.EventFilters.GaussianShapeFilter import GaussianShapeFilter

from datetime import date, timedelta, datetime, time
from inc.helper import DateTimeRange
from inc.helper import GeoBoundingBox
import FrameworkModels

from Toolkit.EventInfoExtractors.Name import Name
from Toolkit.EventInfoExtractors.Time import Time
from Toolkit.EventInfoExtractors.Location import Location
from Toolkit.EventInfoExtractors.OpenStreetMapLink import OpenStreetMapLink
from Toolkit.EventInfoExtractors.WikiLink import WikiLink
from Toolkit.EventInfoExtractors.UserCount import UserCount
from Toolkit.EventInfoExtractors.ContentPreview import ContentPreview
from Toolkit.EventInfoExtractors.RelatedWords import RelatedWords
from Toolkit.EventInfoExtractors.RelatedItems import RelatedItems
from Toolkit.EventInfoExtractors.OccurrencePattern import OccurrencePattern

# event detection class
# Basic class for all event detection algorithms
class EventDetection(object):

	def __init__(self, start_date, end_date, geoBoundingBox, db_file):

		start_date = datetime.combine(start_date, time(0,0,0))
		end_date = datetime.combine(end_date, time(0,0,0))
		self.timeRange = DateTimeRange(start_date, end_date)
		self.geo_bb = geoBoundingBox
		self.db_file = db_file

	# get the content
	# needs to be overwritten by subclass
	def getContent(self):
		raise Exception("abstract method")
	
	# get events from event detection
	def GetData(self, provider):

		EVENTS = []
		# get data
		data = provider.GetContent()

		if not data:
			print "Found no data"
			return EVENTS

		# filter data
		#data = self.filterData(data)

		# detect events
		CLUSTERS = self.eventDetection(data, provider)

		if not CLUSTERS:
			return EVENTS

		# filter clusters
		CLUSTERS = self.filterClusters(CLUSTERS)

		if not CLUSTERS:
			return EVENTS

		# construct events
		EVENTS = self.eventConstruction(CLUSTERS, data, provider)


		if not EVENTS:
			return EVENTS

		# filter events
		EVENTS = self.filterEvents(EVENTS)

		return EVENTS

	# filter the data
	def filterData(self, data):
		dataFilter = RemoveSimilarsFromSameUser(7200,0.5)
		filterdData = []
		for datum in data:
			keep = True
			if dataFilter.filter(datum):
				keep = False
			if keep:
				filterdData.append(datum)
		return filterdData

	# detect events
	def eventDetection(self, data, provider):
		dt_scale = 345600
		geo_lat_scale = 0.0075
		geo_long_scale = 0.0075
		text_dimensions = 100
		text_filter = ["a", "the", "and", "or", "in", "i", "am", "i'am", "of", "with",
								"munich", "munchen", "muenchen", u"münchen",
								"der", "die", "das", "und",
								"2012",]
		text_scale = 0.5
		text_penalty = 999999
		eps = 1
		min_samples = 7

		EVENT_DETECTION_ALGORITHM = TextualDBSCANEventDetectionAlgorithm(dt_scale, geo_lat_scale, geo_long_scale, text_dimensions, text_filter, text_scale, text_penalty, eps, min_samples )


		CLUSTERS = EVENT_DETECTION_ALGORITHM.DetectEvents(data, provider)

		print "\tnumber of clusters: {}".format(len(CLUSTERS))
		for c in CLUSTERS:
			if not(isinstance(c, FrameworkModels.ContentCluster)):
				raise Exception("EVENT_DETECTION_ALGORITHM must return a list of FrameworkModels.ContentClusters, but an entry of type {} was found!".format(type(c)))

		return CLUSTERS
    
	# filter cluster
	def filterClusters(self, clusters):
		return clusters
    
	# contruct events
	def eventConstruction(self, clusters, data, provider):
		EVENT_INFO = [
			Name(0.3,0.3),
			Time(),
			Location(),
			OpenStreetMapLink(),
			WikiLink(),
			UserCount(),
			ContentPreview(),
			RelatedWords(0.1, 0.1) ,
			RelatedItems(),
			OccurrencePattern()
		]
		EVENTS = []
		for c in clusters:
			event_identifier = "UNK"
			event_time = (None, None)
			event_location = {
				"center": (None, None),
				"bbox": {
				"latitude": (None, None),
				"longitude": (None, None)
				}
			}
			event_additionalInfo = {}
			EVENT = FrameworkModels.BasicEvent(event_identifier, event_time, event_location, c, event_additionalInfo)
			for infoExtractor in EVENT_INFO:
				info = infoExtractor.ExtractInfo(data, c, provider, EVENT, EVENTS)

				if isinstance(infoExtractor, FrameworkModels.EventIdentifierExtractor):
					event_identifier = info
				elif isinstance(infoExtractor, FrameworkModels.EventTimeExtractor):
					event_time = info
				elif isinstance(infoExtractor, FrameworkModels.EventLocationExtractor):
					event_location = info
				else:
					infoKey = str(infoExtractor)
					while infoKey in event_additionalInfo:
						infoKey = infoKey + "_"

					event_additionalInfo[infoKey] = info

				EVENT = FrameworkModels.BasicEvent(event_identifier, event_time, event_location, c, event_additionalInfo)
			EVENTS.append(EVENT)
		print "\tnumber of events: {}".format(len(EVENTS))
		return EVENTS

   # filter events
	def filterEvents(self, events):
		EVENT_FILTERS = []
		EVENT_FILTERS.append(ContentClusterSizeThreshold(7))
		EVENT_FILTERS.append(ContributorNumberThreshold(0))
		EVENT_FILTERS.append(GaussianShapeFilter(0.845))

		filteredEVENTS = []
		for EVENT in events:
			skip = False
			for filter in EVENT_FILTERS:
				if filter.filter(EVENT):
					#print "\t.. event '" + EVENT.Identifier.encode("utf-8") + "' skipped due to '" + str(filter) + "'"
					skip = True
					break
			if skip:
				continue
			else:
				filteredEVENTS.append(EVENT)

		return filteredEVENTS
