# -*- encoding: utf-8-*-
from Toolkit.SocialMediaContentProviders.Facebook import FacebookContentProvider
from  EventDetection import EventDetection


# detect facebook events
# subclass of EventDetection
class FacebookEventDetection(EventDetection):

	def __init__(self, event_name, start_date, end_date, geoBoundingBox):
		EventDetection.__init__(self, start_date, end_date,geoBoundingBox, "twitter.sqlite")
		self.event_name = event_name

	def getContent(self):
		provider = FacebookContentProvider(self.timeRange, self.geo_bb, self.event_name, self.db_file)
		return self.GetData(provider)
