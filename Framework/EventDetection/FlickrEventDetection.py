# -*- encoding: utf-8-*-
from Toolkit.SocialMediaContentProviders.Flickr import FlickrContentProvider
from  EventDetection import EventDetection

# detect flickr events
# subclass of EventDetection
class FlickrEventDetection(EventDetection):
	def __init__(self, start_date, end_date, geoBoundingBox):
		EventDetection.__init__(self, start_date, end_date,geoBoundingBox, "databases/Flickr.sqlite")


	def getContent(self):
		provider = FlickrContentProvider(self.timeRange, self.geo_bb, "datetime_taken", self.db_file)
		data = self.GetData(provider)
		return data
