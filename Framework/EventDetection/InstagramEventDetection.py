# -*- encoding: utf-8-*-
from Toolkit.SocialMediaContentProviders.Instagram import InstagramContentProvider
from  EventDetection import EventDetection
from inc.helper import GeoBoundingBox

# detect instagram events
# subclass of EventDetection
class InstagramEventDetection(EventDetection):

	def __init__(self, start_date, end_date, geoBoundingBox):
		EventDetection.__init__(self, start_date, end_date, geoBoundingBox, "Instagram.sqlite")


	def getContent(self):
		query = "München"
		provider = InstagramContentProvider(self.timeRange, self.geo_bb,self.db_file)
		return self.GetData(provider)
