# -*- encoding: utf-8-*-
from Toolkit.SocialMediaContentProviders.Twitter import TwitterContentProvider
from  EventDetection import EventDetection

# detect twitter events
# subclass of EventDetection
class TwitterEventDetection(EventDetection):

	def __init__(self, event_name, start_date, end_date, geoBoundingBox):
		EventDetection.__init__(self, start_date, end_date, geoBoundingBox, "databases/twitter.sqlite")
		self.event_name = event_name

	def getContent(self):
		provider = TwitterContentProvider(self.timeRange, self.geo_bb, self.event_name, self.db_file)
		return self.GetData(provider)
