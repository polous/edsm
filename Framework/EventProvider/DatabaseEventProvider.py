# -*- coding: utf-8 -*-

from django.conf import settings
import psycopg2
import psycopg2.extras
import math
import json
import sys
import datetime
from osmapi import OsmApi
from nominatim import Nominatim, NominatimReverse
#from datetime import datetime
from FrameworkModels import BasicEvent
from inc.helper import GeoLocation
from inc.helper import DateTimeRange
from FrameworkModels import BasicContent
from FrameworkModels import ContentCluster


class DatabaseEventProvider():

    """
    Represents the events from the database
    This class is used to get events and additional data from the database
    or store events with data to database.
    """

    def __init__(self):
        try:
            db_name = settings.EVENT_DATABASE['NAME']
            db_host = settings.EVENT_DATABASE['HOST']
            db_user = settings.EVENT_DATABASE['USER']
            db_pw = settings.EVENT_DATABASE['PW']
            db_port = settings.EVENT_DATABASE['PORT']

            self.dbConnection = psycopg2.connect(database = db_name, user = db_user, password = db_pw, host = db_host, port=db_port)

        except psycopg2.DatabaseError, e:
            print 'Error %s' %e
            sys.exit(1)

    def getEvent(self, eventid):
        """
            Get a specific event from the database 
            param: eventid  integer The id of the event which is searched
            returns: list of BasicEvents with this ID (normally only one event)
        """
        cur = self.dbConnection.cursor(cursor_factory=psycopg2.extras.DictCursor)

        cur.execute("SELECT * FROM search_event where id =" + eventid)

        rows = cur.fetchall()

        Events = []
        #for row in rows
        for row in rows:
            currEvent = self.createEventFromDatabaseRow(row)
            Events.append(currEvent)

        return Events

    # get list of events
    def getEvents(self, name, starttime, endtime, category, geoboungingbox):
        """
            Get the events from the database. 
            Searches through the database for events with given parameters. 
            It also searches in the tables for social media stuff like tweets or flickr posts

            params: name String the name of the search. Every result will contain this name case insensitive
                    starttime datetime.date The startdate where the events should BasicEvent
                    endtime   datetime.date The enddate where the events have to be ended latest
                    category String The category which should be searched
                    geoboundingbox GeoBoundingBox contains the 4 corner points of the bounding box 
            returns: list of BasicEvents which are found inside the database
        """

	name = name.encode("utf-8")
	
        minLat = convCoordToDatabaseNumber(geoboungingbox.latitude[0])
        minLon = convCoordToDatabaseNumber(geoboungingbox.longitude[0])
        maxLat = convCoordToDatabaseNumber(geoboungingbox.latitude[1])
        maxLon = convCoordToDatabaseNumber(geoboungingbox.longitude[1])

        # Building query for search in database
        query = "SELECT * FROM search_event WHERE "
        query += "latitude >= %(minLat)s and latitude <= %(maxLat)s and longitude >= %(minLon)s and longitude <= %(maxLon)s  " #.format(minLat, maxLat, minLon, maxLon)
        query_data = {"minLat": minLat, "maxLat" : maxLat, "minLon" : minLon, "maxLon" : maxLon }
        if name != "":
            query += "and (name ILIKE '%%{0}'  OR name ILIKE '{0}%%' OR name ILIKE '%%{0}%%' ) ".format( name)

        if starttime != "":
            starttime_array = starttime.split("/")
            start_day = starttime_array[0]
            start_month = starttime_array[1]
            start_year = starttime_array[2]
            starttime = start_month + "/" + start_day + "/" + start_year

        if endtime != "":
            print endtime
            endtime_array = endtime.split("/")
            endtime_day = endtime_array[0]
            endtime_month = endtime_array[1]
            endtime_year = endtime_array[2]
            endtime = endtime_month + "/" + endtime_day + "/" + endtime_year
            print endtime

        
        if starttime != "" and endtime != "":
            starttime_array = starttime.split("/")
            endtime_array = endtime.split("/")

            start_day = starttime_array[1]
            start_month = starttime_array[0]

            endtime_day = endtime_array[1]
            endtime_month = endtime_array[0]

            query += "and ( startdate >= %(starttime)s and enddate <= %(endtime)s  or howoften = 'Daily' or \
                    (howoften = 'Yearly' and date_part('month', startdate) >= %(start_month)s and date_part('day', startdate) >= %(start_day)s and date_part('month', enddate) <= %(end_month)s and date_part('day', enddate) <= %(end_day)s) \
                    or (howoften = 'Monthly' and date_part('day', startdate) >= %(start_day)s and date_part('day', enddate) <= %(end_day)s)) "
            query_data["starttime"] = starttime
            query_data["endtime"] = endtime
            query_data["start_month"] = start_month
            query_data['start_day'] = start_day
            query_data['end_month'] = endtime_month
            query_data["end_day"] = endtime_day
        elif starttime != "" and endtime == "":
            query += "and (startdate >= %(starttime)s or howoften = 'Daily' or howoften = 'Monthly' or howoften = 'Yearly') "
            query_data["starttime"] = starttime
        elif starttime == "" and endtime != "":
            query += "and enddate <= %(endtime) or howoften = 'Daily' or howoften = 'Monthly' or howoften = 'Yearly') "
            query_data["endtime"] = endtime

        if category != "" and category != "all":
            print "Found category:"
            print category
            query += "and (category ILIKE '%%{0}'  OR category ILIKE '{0}%%' OR category ILIKE '%%{0}%%' ) ".format( category)

        cur = self.dbConnection.cursor(cursor_factory=psycopg2.extras.DictCursor)

        # run the query
        print cur.mogrify(query, query_data)
        cur.execute(query, query_data)
        
        rows = cur.fetchall()

        Events = []
        for row in rows:
            # every result row needs to be converted into a BasicEvent object
            currEvent = self.createEventFromDatabaseRow(row)
            if currEvent:
                Events.append(currEvent)

        return Events
    
    def createEventFromDatabaseRow(self, databaseRow):
        """
            Create a BasicEvent from a database row
            params: databaseRow Dictionary Containing database row coming from a database search inside the search_event table
            returns: BasicEvent with ContentCluster
        """
        lat = convDatabaseNumberToCoord(databaseRow["latitude"])
        lon = convDatabaseNumberToCoord(databaseRow["longitude"])

        location = {
            'center': [ lat, lon],
            'bbox': {
                'latitude' : [ lat,lat],
                'longitude': [ lon, lon]
            }
        }
        geoLocation = GeoLocation(lat,lon)
        dtRange = []

        # if startdate is not in database row
        if not databaseRow["startdate"]:

            #check if enddate is in database row
            # if true then use enddate also as startdate
            if databaseRow["enddate"]:
                dtRange.append(databaseRow["enddate"])
            #else return no object because there is no time defined
            else: 
                return
        else:
            dtRange.append(databaseRow["startdate"])
        
        # if enddate is empty insert startdate as enddate
        if not databaseRow["enddate"]:
            dtRange.append(databaseRow["startdate"])
        else:
            dtRange.append(databaseRow["enddate"])
        
        basicContent = BasicContent(databaseRow["id"], databaseRow["name"], 1, geoLocation, "", databaseRow["url"])
        contentCluster = []

        # add aditional info
        additionalInfo = {}
        additionalInfo["category"] = databaseRow["category"]
        additionalInfo["subcategory"] = databaseRow["subcategory"]
        additionalInfo["url"] = databaseRow["url"]
        additionalInfo["num_participants"] = databaseRow["num_participants"]
        additionalInfo["howoften"] = databaseRow["howoften"]

        # get additional social media info and comments from social_media_events database
        addInfo = self.getAdditionalDataForEvent(databaseRow["social_media_id"])
        additionalInfo["social_media"] = addInfo["social_media"]
        additionalInfo["comments"] = addInfo["comments"]

        # search for tweets
        tweets = self.getTweetsForEvent(databaseRow["social_media_id"])
        if len(tweets) != 0 :
            print "Has tweets"
            startIndex = len(contentCluster)
            additionalInfo["HasTweets"] = True
            additionalInfo["TweetsStartIndex"] = startIndex
            additionalInfo["TweetsEndIndex"] = startIndex+len(tweets) - 1
            contentCluster.extend(tweets)
        else:
            additionalInfo["HasTweets"] = False
        
        # search for flickr posts
        flickr_posts = self.getFlickrPostsForEvent(databaseRow["social_media_id"])
        if len(flickr_posts) != 0 :
            print "Found posts"
            startIndex = len(contentCluster)
            additionalInfo["HasFlickrPosts"] = True
            additionalInfo["FlickrStartIndex"] = startIndex
            additionalInfo["FlickrEndIndex"] = startIndex+len(flickr_posts) - 1
            contentCluster.extend(flickr_posts)
        else:
            additionalInfo["HasFlickrPosts"] = False

        # construct BasicEvent
        currEvent = BasicEvent( databaseRow["name"], dtRange, location, contentCluster, additionalInfo )
        return currEvent
    

    def getTweetsForEvent(self, event_id):
        """
            Get all tweets for one event from the database
            params: event_id integer The event id which should be searched
            returns: List of BasicContent containing the tweets informations
        """
        cur = self.dbConnection.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("SELECT * FROM twitter_tweets WHERE event_ids @> ARRAY[%s]::integer[]", [event_id] )
        #cur.execute("SELECT * FROM Twitter_Tweets WHERE event_ids = %s;", [event_id])
        tweets = cur.fetchall()
        contentCluster = []
        for tweet in tweets:
            #id, title, timestamp, geo_location, publisher, url
            geoLocation = GeoLocation(tweet["latitude"], tweet["longitude"])
            content = BasicContent(tweet["id"], tweet["tweet_text"], tweet["timestamp"], geoLocation, "", tweet["url"])
            contentCluster.append(content)
        return contentCluster


    def getFlickrPostsForEvent(self, event_id):
        """
            Get all flickr posts for one event from the database
            params: event_id integer The event id which should be searched
            returns: List of BasicContent containing the flickr posts informations
        """
        cur = self.dbConnection.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("SELECT * FROM flickr_posts WHERE event_ids @> ARRAY[%s]::integer[]", [event_id])
        posts = cur.fetchall()
        contentCluster = []
        for post in posts:
            #id, title, timestamp, geo_location, publisher, url
            geoLocation = GeoLocation(post["latitude"], post["longitude"])
            content = BasicContent(post["post_id"], post["text"], post["timestamp"], geoLocation, "", post["url"])
            contentCluster.append(content)
        return contentCluster


    def getAdditionalDataForEvent(self, event_id):
        """
            Get the additional data for an event from the social_media_events table.
            This table contains comments and social media information.
            If one event is found multiple times inside the table than the comments are concatenated
            params: event_id String the event id for the event
            returns: Dictionary containing the social media type and the comments
        """
        data = {}
        social_media = ""
        comments = ""
        cur = self.dbConnection.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("SELECT * FROM social_media_events WHERE id = %s;", [event_id])

        rows = cur.fetchall()

        #if more than one social media do not show any icon. so social media is empty
        if len(rows) > 0:
            social_media = rows[0][1]

        for row in rows:
            print row
            if row[0] != social_media:
                social_media = ""
            
            if row[1]:
                print "Comment:"
                print row[2]
                if comments:
                    comments += ", " 
                if row[1] != "":
                    comments += row[1]

        data["social_media"] = social_media
        data["comments"] = comments
        return data

    def storeEventToDatabase( self, data):
        """
            Method for saving an event to the database
            It saves the event into the search_event table and all additional information to the other tables
            params: data QueryDict coming from the request for saving an event
            returns: Dictionary with error code and message
        """
        dataDict =  data.dict()
        social_media = dataDict["SocialMedia"]
        event_start = datetime.datetime.strptime(dataDict["StartDate"], '%Y-%m-%dT%H:%M:%S')
        event_end = datetime.datetime.strptime(dataDict["EndDate"], '%Y-%m-%dT%H:%M:%S')

        lat = float(dataDict["Lat"])
        lon = float(dataDict["Lon"])

        event_lat_db = convCoordToDatabaseNumber(lat)
        event_lon_db = convCoordToDatabaseNumber(lon)
        event_name = dataDict["EventName"]
        event_comment = dataDict["Comment"]
        event_how_often = dataDict["HowOften"]
        print "How often:"
        print event_how_often
        number = self.getNumber(event_lat_db, event_lon_db, event_start.date())

        additionalInfoFromJson = json.loads(dataDict["AdditionalInfo"])
        contentClusterFromJson = json.loads(dataDict["ContentCluster"])

        contentCluster = []

        for jsonEntry in contentClusterFromJson:
            lat = jsonEntry["Latitude"]
            lon = jsonEntry["Longitude"]

            geoLocation = GeoLocation(lat, lon)

            content = BasicContent(jsonEntry["ID"], jsonEntry["Title"], jsonEntry["TimeStamp"], geoLocation, jsonEntry["Publisher"], jsonEntry["URL"] )
            contentCluster.append(content)

        num_participants = additionalInfoFromJson["UserCount"]

        url = ""
        if "Url" in additionalInfoFromJson:
            url = additionalInfoFromJson["Url"]

        api_osm = OsmApi(api="http://osm.openeventmap.tum.de")
    
        #needed for map search on OsmApi
        minLon = lon - 0.0000001
        maxLon = lon + 0.0000001
        minLat = lat - 0.0000001
        maxLat = lat + 0.0000001

        related_items = self.getRelatedItems(lat, lon)
        search_result =  api_osm.Map( minLon, minLat,maxLon,maxLat)
        existingEventID = self.checkIfEventAlreadyInDB(event_name, event_lat_db, event_lon_db, event_start.date(), event_end.date())
        if existingEventID == -1:
            cur = self.dbConnection.cursor(cursor_factory=psycopg2.extras.DictCursor)
            cur.execute("INSERT INTO social_media_events( comment, social_media) \
                VALUES( %s, %s ) RETURNING id;" \
                , ( event_comment, social_media ))
            # get id of created event
            event_id = cur.fetchone()[0]
            

            if not search_result:
                # create new node
                related_items = self.getRelatedItems(lat, lon)
                self.createNewOSMNode(event_name, dataDict["EventCategory"], dataDict["EventSubcategory"],event_start, event_end, lat, lon, url, num_participants, related_items, event_id, "Once", event_comment )
                self.insertContentClusterToDatabase(event_id, contentCluster, social_media)
                self.dbConnection.commit()
            else:
                tags = search_result[0]["data"]["tag"]
                updated_tags_dict = {}
                # get number tags
                if 'event' in tags:
                    if tags['event'] == "yes":
                        for key, value in tags.iteritems():
                            if key.partition(':')[-1].rpartition(':')[0]:
                                old_number = int(key.partition(':')[-1].rpartition(':')[0])
                                if number <= old_number:
                                    new_key = key.partition(':')[0] + ":" + str(old_number + 1) + ":" + key.rpartition(':')[2]
                                    new_value = value
                                    updated_tags_dict[new_key] = value
                                else:
                                    updated_tags_dict[key] = value
            
                #add tags of new event
                related_items = self.getRelatedItems(lat, lon)
                event_tags = self.eventTags(event_name, number, dataDict["EventCategory"], dataDict["EventSubcategory"], event_start, event_end, related_items, url, num_participants, event_how_often, event_comment, "123" )
                new_tags_dict = dict(updated_tags_dict.items() + event_tags.items())
                updateDict= {}
                updateDict["lat"] = float(search_result[0]["data"]["lat"])
                updateDict["lon"] = float(search_result[0]["data"]["lon"])
                updateDict["tag"] = new_tags_dict
                updateDict["id"] = int(search_result[0]["data"]["id"])
                updateDict["version"] = int(search_result[0]["data"]["version"])
                self.changeNode( updateDict)

                self.insertContentClusterToDatabase(existingEventID, contentCluster, social_media)
                self.dbConnection.commit()
        else:
            #only add comment and content
            self.insertContentClusterToDatabase(existingEventID, contentCluster, social_media)
            self.insertIntoSocialMediaDB(existingEventID, social_media, event_comment)

            self.dbConnection.commit()
            return {"error" : 0, "message" : "Event already in database. Added only the comment and the social media data!"}

        return {"error" : 1, "message" : "Successfully saved event to Openstreetmap"}

    def createNewOSMNode(self, name, category, subcategory, startdate, enddate, lat, lon, url, num_participants, related_items, social_media_id, how_often, comment):
        """
            Method to create new OSM node
            It creates a changeset first and then adds the node to osm
        """
        
        # create changeset over api
        api_osm = OsmApi(api="http://osm.openeventmap.tum.de", username="openeventmaptum", password="idp12345")
        changesetid = api_osm.ChangesetCreate({u"comment": u"Openeventmap creating new node"})
    
        #({u"lon":1, u"lat":1, u"tag": {}})
        new_node = {}
        tags = self.eventTags(name, 0, category, subcategory, startdate, enddate, related_items, url, num_participants, how_often, comment, social_media_id)
        new_node[toUnicode("lon")] = lon
        new_node[toUnicode("lat")] = lat
        new_node[toUnicode("tag")] = tags

        imp_result = api_osm.NodeCreate(new_node)

        osm_id = imp_result["id"]

        target = open("osm_create.txt", 'a')
        target.write("########################")
        target.write("\n")
        target.write("Creating new node:")
        target.write("\n")
        target.write("\n")
        target.write(str(new_node))
        target.write("\n")
        target.write("\n")
        target.write("OSM ID: " + str(osm_id))
        target.write("\n")
        target.write("\n")
        target.write("ChangesetID: " + str(changesetid))
        target.write("\n")
        target.write("########################")
        target.write("\n")
        target.write("\n")
        target.close()
        api_osm.ChangesetClose()
        
        print new_node
    
    """
       Get related items
    """
    def getRelatedItems(self, lat, lon):
        nomrev = NominatimReverse()
        nom_result = nomrev.query(lat=lat, lon = lon)
        return "{0}: {1}".format(nom_result["osm_type"].title(), nom_result["osm_id"])


    def changeNode(self, osm_xml):
        """
            change osm node 
        """
            # create changeset over api
        api_osm = OsmApi(api="http://osm.openeventmap.tum.de", username="openeventmaptum", password="idp12345")
        changesetid = api_osm.ChangesetCreate({u"comment": u"Openeventmap updating node"})
    
        imp_result = api_osm.NodeUpdate(osm_xml)

        osm_id = imp_result["id"]

        target = open("osm_update.txt", 'a')
        target.write("########################")
        target.write("\n")
        target.write("Updating node:")
        target.write("\n")
        target.write("\n")
        target.write(str(osm_xml))
        target.write("\n")
        target.write("\n")
        target.write("OSM ID: " + str(osm_id))
        target.write("\n")
        target.write("\n")
        target.write("ChangesetID: " + str(changesetid))
        target.write("\n")
        target.write("########################")
        target.write("\n")
        target.write("\n")
        target.close()
        api_osm.ChangesetClose()

    
    def insertIntoSocialMediaDB(self, event_id, social_media, comment):
        """
            Add the social media information and the comment to the social_media_events table
            param: event_id integer The id of the event
                    social_media String the type of the social media (like "twitter" or "flickr")
                    coment  String The comment which the user has entered
        """
        cur = self.dbConnection.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("INSERT INTO social_media_events( event_id, social_media, comment  ) \
                VALUES(%s, %s, %s);"  \
                , ( event_id, social_media, comment ))


    def insertContentClusterToDatabase(self, event_id, contentCluster, social_media):
        """
            Add the social media content to the tables
            param: event_id integer The id of the event
                   contentCluster List of BasicContent The content which should be added
                   social_media String the social media type
        """
        if social_media == "Twitter":
            self.insertTweets(event_id, contentCluster)
        elif social_media == "Flickr":
            self.insertFlickrPosts(event_id, contentCluster)

    def insertTweets(self, event_id, tweets):
        """
            Insert tweets into twitter_tweets table
            Tweets are only inserted if no tweet with same id is in table
            If tweet already exists and the event id is not in the events_ids list of the tweet than 
            the eventid is added to this tweet
            param: event_id integer The id of the event
                   tweets List of BasicContent with tweets
        """
        cur = self.dbConnection.cursor(cursor_factory=psycopg2.extras.DictCursor)

        for content in tweets:
            lat = convCoordToDatabaseNumber(content.GeoLocation.latitude)
            lon = convCoordToDatabaseNumber(content.GeoLocation.longitude)

            cur.execute("SELECT id, event_ids from twitter_tweets WHERE id = %s", [content.Id])

            if cur.rowcount == 0:
                lat = convCoordToDatabaseNumber(content.GeoLocation.latitude)
                lon = convCoordToDatabaseNumber(content.GeoLocation.longitude)
                cur.execute("INSERT INTO twitter_tweets( tweet_text, url, event_ids, id, timestamp, latitude, longitude  ) \
                    VALUES(%s, %s, %s, %s, %s, %s, %s);"  \
                    , ( content.Title, content.URL, [event_id], content.Id, content.TimeStamp, lat, lon ))           
            else:    
                res = cur.fetchone()
                print "tweet already exists"
                #only add id of current event to list
                if not event_id in res["event_ids"]:
                    # add event id to list
                    cur.execute(" UPDATE twitter_tweets SET event_ids = array_append(event_ids, %s) WHERE id = %s ;", (event_id, content.Id))


    def insertFlickrPosts(self, event_id, posts):
        """
            Insert flickr posts into flickr_posts table
            Posts are only inserted if no post with same id is in table
            If post already exists and the event id is not in the events_ids list of the post than 
            the eventid is added to this post
            param: event_id integer The id of the event
                   posts List of BasicContent with posts
        """
        cur = self.dbConnection.cursor(cursor_factory=psycopg2.extras.DictCursor)
        for post in posts:
            cur.execute("SELECT post_id, event_ids from flickr_posts WHERE post_id = %s", [post.Id])

            if cur.rowcount == 0:
                lat = convCoordToDatabaseNumber(post.GeoLocation.latitude)
                lon = convCoordToDatabaseNumber(post.GeoLocation.longitude)
                
                cur.execute("INSERT INTO flickr_posts( text, url, event_ids, post_id, timestamp, latitude, longitude  ) \
                    VALUES(%s, %s, %s, %s, %s, %s, %s);"  \
                    , ( post.Title, post.URL, [event_id], post.Id, post.TimeStamp, post.GeoLocation.latitude, post.GeoLocation.longitude ))
            else:
                res = cur.fetchone()
                print "post already exists"
                #only add id of current event to list
                print res["event_ids"]
                if event_id in res["event_ids"]:
                    print "already in"
                else:
                    # add event id to list
                    cur.execute(" UPDATE flickr_posts SET event_ids = array_append(event_ids, %s) WHERE post_id = %s ;", (event_id, post.Id))
                    
    def eventTags(self, name, number, category, subcategory, startdate, enddate, related_items, url, num_participants, howoften, comment, social_media_id ):
        """
            Get events tag 
        """
        
        tags = {}

        print "related items in tags " + related_items

        baseTag = "event:" + str(number) + ":"
        tags[toUnicode("event")] = toUnicode("yes")
        tags[toUnicode(baseTag + "name")] = toUnicode(name)
        tags[toUnicode(baseTag + "category")] = toUnicode(category)
        tags[toUnicode(baseTag + "subcategory")] = toUnicode(subcategory)
        tags[toUnicode(baseTag + "startdate")] = toUnicode("{:%d.%m.%Y-%H:%M}".format(startdate))
        tags[toUnicode(baseTag + "enddate")] = toUnicode("{:%d.%m.%Y-%H:%M}".format(startdate))
        tags[toUnicode(baseTag + "related_items")] = toUnicode(related_items)
        tags[toUnicode(baseTag + "url")] = toUnicode(url)
        tags[toUnicode(baseTag + "num_participants")] = toUnicode(convNumParticipantsToDBValue( num_participants))
        tags[toUnicode(baseTag + "howoften")] = toUnicode("Once")
        tags[toUnicode(baseTag + "comment")] = toUnicode(comment)
        tags[toUnicode(baseTag + "social_media_id")] = toUnicode(str(social_media_id))
        return tags
    

    def generateXMLTag(self,key,value):
        """
            Generate xml tag for osm
        """
        return '<tag k="{0}" v="{1}" />'.format(key, value) 


    def getNumber(self,lat, lon, event_start_date):
        """
            Helper function for getting the correct "number" for one event.
            The number is ordered. Events taking place at the same location are related. 
            The event which is coming first get 0, the next gets 1 and so on. So they are ordered by start date.
            If a new event is inserted into the database it could be possible that it needs to get a number somewhere between two other events.
            If this happends all other entries need to be reorderd to fulfill the time contraints. 
            param:  lat integer Latitude postition of the event
                    lon integer Longitude postiton of the event
                    event_start_date datetime The startdate of the event
            returns: the "number" for the new event
        """

        cur = self.dbConnection.cursor(cursor_factory=psycopg2.extras.DictCursor)

        #search for events on this location
        cur.execute("SELECT * FROM \
            search_event where    \
            latitude = %s and  longitude = %s \
        ", (lat, lon))

        rows = cur.fetchall()
        number = 0

        # if event is found than search the correct number for the new event
        if len(rows) > 0:
            from operator import itemgetter
            #sort results by seventh entry (that is the start_date)
            sortedRows = sorted(rows, key=itemgetter(7))
            current_start_date = event_start_date

            #convert start datetime to date
            if isinstance(current_start_date, datetime.datetime):
                current_start_date = current_start_date.date()
            
            for row in sortedRows:
                if row[7] < current_start_date:#.date():
                    number += 1
            
            # if number is not the last one all other entries need to be reordered.
            #if number != (len(rows) - 1):
             #   self.reorderEntries( sortedRows, number  )

            return number
        else:
            print 0
            return number
    

    def checkIfEventAlreadyInDB(self, name, lat, lon , startDate, endDate):
        """
            Helper function for testing if event is already in database.
            It tests if there is an event in the database with the same name, location and time range
            params: name String The name of the event
                    lat integer The latitude of the event
                    lon integer The longitude of the event
                    startDate datetime.date The startdate
                    endDate datetime.date The enddate
            returns: -1 if no event was found or the event_id of the found event
        """
        cur = self.dbConnection.cursor(cursor_factory=psycopg2.extras.DictCursor)

        cur.execute("SELECT * FROM \
            search_event where    \
            name = %s and \
            startdate = %s and enddate = %s and \
            latitude = %s and longitude = %s \
        ", (name, startDate, endDate, lat, lon))

        if cur.rowcount == 0:
            print "Not found"
            return -1
        else:
            print "Found"
            rows = cur.fetchall()
            eventid = rows[0]["id"]
            return eventid


def convDatabaseNumberToCoord(number):
    """
        Converts a database number to a valid coordinate
        params: number integer The database number
        returns: integer The valid coordinate
    """
    digits = int(math.log10(number))+1
    missing_digits = 9 - digits
    if missing_digits >= 0:
        number = number * math.pow(10, missing_digits)
    else:
        number =  number / math.pow(10, math.fabs(missing_digits))

    factor = 10000000
    result = number / factor + float(number - (number / factor) * factor) / factor
    return result

def convCoordToDatabaseNumber(number):
    """
        Converts a coordinate to a database number
        params: number integer The coordinate
        returns: integer The database number
    """
    digits = int(math.log10(int(number)))+1
    missing_digits = 9 - digits
    if missing_digits >= 0:
        number = number * math.pow(10, missing_digits)

    else:
        number =  number / math.pow(10, math.fabs(missing_digits))

    return int(round(number))



def convNumParticipantsToDBValue(num_participants):
    """
        Convertsthe number of participants to string
    """
    if num_participants < 50:
        return "less than 50"
    elif num_participants < 200:
        return "50 to 200"
    elif num_participants < 1000:
        return "200 to 1000"
    elif num_participants < 10000:
        return "1000 to 10000"
    else:
        return "more than 10000"


def toUnicode(input_string):
    """
        Converts string to unicode
    """
    if not isinstance(input_string, unicode):
        return unicode(input_string, "utf-8")
    else:
        return input_string

