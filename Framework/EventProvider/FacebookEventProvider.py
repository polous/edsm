from FrameworkModels import BasicEvent
from inc.helper import GeoLocation
from inc.helper import DateTimeRange
from inc import algos,helper
from FrameworkModels import BasicContent
from FrameworkModels import ContentCluster
import facebook
from datetime import datetime
from django.utils.dateparse import parse_datetime

APP_ID = '1075761215812214'
APP_SECRET = 'c17a708801cd89c857f1de90618edcfd'
#User Access Token will expire 17. Januar 2017
ACCESS_TOKEN = "EAAUgz7JURZBwBADiM9eNWz5Tg4E66QMPCzOmGch15KpU8Lmo05cHumIZBLhO0uuHjc8H3746zBdIfOlu2jx0vwqQbdm1P3uihXEx6mT9R0XQEs63usNrQ13KA9MT6TakPE0iVtALrtAigTtzhJ5MRZALP33kDWnZBjcK0oAHZBwZDZD"
URL = "https://facebook.com/events/{event_id}"


class FacebookEventProvider():
    """
        Class for providing facebook events
    """


    def __init__(self):
        """
        """
        self.graph = facebook.GraphAPI(access_token=ACCESS_TOKEN, version='2.7')
    
    
    def getEvents(self,name,starttime, endtime, geoBoundingBox):
        """
            Get events in with given name, time range and bounding box
        """
        geo_lat_center = geoBoundingBox.latitude[0] + (geoBoundingBox.latitude[1]-geoBoundingBox.latitude[0])/2.
        geo_lon_center = geoBoundingBox.longitude[0] + (geoBoundingBox.longitude[1]-geoBoundingBox.longitude[0])/2.
        geo_radius = max(algos.latlon_distance((geoBoundingBox.latitude[0], geo_lon_center), (geoBoundingBox.latitude[1], geo_lon_center)), algos.latlon_distance((geo_lat_center, geoBoundingBox.longitude[0]), (geo_lat_center, geoBoundingBox.longitude[1])))

        placeIds = self.getPlaceIds(geo_lat_center, geo_lon_center, geo_radius)
        #print "Places id list: " + str(placeIds)

        events = self.getEventsFromPlaces(placeIds)

        Events = []
        for placeId in placeIds:
            #print "Searching: " + placeId
            placeDict = events[placeId]
            #print "PlaceDict: " + str(placeDict)
            if 'events' in placeDict:
                for event in placeDict['events']['data']:
                    search_starttime = starttime 
                    search_endtime = endtime 
                    event_start_time = datetime.strptime(event['start_time'][:event['start_time'].find("T")], '%Y-%m-%d').date()
                    event_end_time = None
                    if 'end_time' not in event:
                        event_end_time = event_start_time
                    else:
                        event_end_time = datetime.strptime(event['end_time'][:event['end_time'].find("T")], '%Y-%m-%d').date()
                        
                    if search_starttime >= event_end_time or search_endtime <= event_start_time:
                        continue

                    location = None
                    if 'place' in event:
                        if 'location' in event['place']:
                            print "in time range"
                            dtRange = (event_start_time, event_end_time)

                            lat = event['place']['location']['latitude']
                            lon = event['place']['location']['longitude']
                            location = {
                                'center': [ lat, lon],
                                'bbox': {
                                    'latitude' : [ lat,lat],
                                    'longitude': [ lon, lon]
                                }
                            }

                    if location is not None :
                        print "Current Event: " + str(event)
                        contentCluster = []
                        additionalInfo = {}

                        additionalInfo["UserCount"] = event['attending_count']
                        if 'category' in event:
                            print event['category']
                            additionalInfo["Category"] = event['category']
                        else:
                            additionalInfo["Category"] = "Unknown"
                        if name in event['name']: 
                            Events.append(BasicEvent(event['name'], dtRange, location, contentCluster, additionalInfo))

        print "Number of events: " + str(len(Events))
        return Events

    def getPlaceIds(self, geo_lat_center, geo_lon_center, geo_radius ):
        """
            get place ids dor location
        """
        results = self.graph.request('search', {'type': 'place', 'center':  str(geo_lat_center) + "," + str(geo_lon_center), 'distance': str(geo_radius * 1000), 'limit':'1000', 'fields': 'id, location' })
        places = dict(results)
        placeIds = []
        for result in places['data']:
            placeIds.append(result['id'])
        return placeIds

    def getEventsFromPlaces(self, placeIds ):
        """
            get events from specified places
        """
        
        max_ids = 50
        idLists = []
        counter = 0
        for placeId in placeIds:
            listIndex = counter / max_ids
            if listIndex >= len(idLists):
                idLists.append("")
            if idLists[listIndex] != "" :
                idLists[listIndex] += "," + placeId
            else:
                idLists[listIndex] += placeId
            counter += 1

        eventsDict = {}
        for ids in idLists:
            events = self.graph.request('',{'ids' : ids, 'fields': 'id,events.fields(id,name, start_time, end_time, attending_count, place, category)' })
            eventsDict.update(dict(events))
        #print "Events: " + str(eventsDict)
        return eventsDict
