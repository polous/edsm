# -*- coding: utf-8 -*-
from FrameworkModels import BasicEvent
from inc.helper import GeoLocation
from inc.helper import DateTimeRange
from inc import algos,helper
from FrameworkModels import BasicContent
from FrameworkModels import ContentCluster
import oauthlib.oauth1 as oauth
import urlparse
import json
from datetime import datetime
import dateutil.parser

from requests_oauthlib import OAuth1Session

XING_CONSUMER_KEY = "25dae2b7bfb5302d4dc1"
XING_CONSUMER_SECRET = "2a6e91d9621b05888d48ecaed1485f2ba86ae778"
XING_ACCESS_TOKEN = "bafaf8ab3652b540e5e0"
XING_ACCESS_SECRET = "bf643aff2e4aee0ee200"

XING_BASE_URL='https://api.xing.com/'


class XingEventProvider():
    """
        Class for providing xing events
    """
    def __init__(self):
        self.i = 1
        self.client = OAuth1Session( XING_CONSUMER_KEY, client_secret=XING_CONSUMER_SECRET, resource_owner_key=XING_ACCESS_TOKEN,resource_owner_secret=XING_ACCESS_SECRET, signature_method=oauth.SIGNATURE_PLAINTEXT)

    def getEvents(self,name,starttime, endtime, geoBoundingBox):
        """
            Get events from xing
        """
        Events = []

        geo_lat_center = geoBoundingBox.latitude[0] + (geoBoundingBox.latitude[1]-geoBoundingBox.latitude[0])/2.
        geo_lon_center = geoBoundingBox.longitude[0] + (geoBoundingBox.longitude[1]-geoBoundingBox.longitude[0])/2.
        geo_radius = int(max(algos.latlon_distance((geoBoundingBox.latitude[0], geo_lon_center), (geoBoundingBox.latitude[1], geo_lon_center)), algos.latlon_distance((geo_lat_center, geoBoundingBox.longitude[0]), (geo_lat_center, geoBoundingBox.longitude[1]))))

        if geo_radius == 0:
            geo_radius = 1

        search_url = XING_BASE_URL + 'v1/events/find.json?'

        #add name to url
        search_url += "keywords=" + name

        #add location
        location = (geo_lat_center, geo_lon_center, int(geo_radius))
        search_url += ("&location=%s,%s,%s" % location)
        search_url += "&limit=100"
        #client = oauth.Client(self.consumer, self.access_token) 

        print search_url

        response = self.client.get(search_url)
        jsonResult = json.loads(response.text)
	
        for event in jsonResult["events"]["items"]:
            event_name = event["title"].encode('utf-8')
            start_date = dateutil.parser.parse(event["start"]["timestamp"]).date()
            
            if event["end"]["timestamp"] is None:
                end_date = start_date
            else:
                end_date = dateutil.parser.parse(event["end"]["timestamp"]).date()
            

            search_starttime = starttime #datetime.strptime(starttime, '%d/%m/%Y').date()
            search_endtime = endtime #datetime.strptime(endtime, '%d/%m/%Y').date()

            # continue if not in time range
            if search_starttime >= end_date or search_endtime <= start_date:
                continue
            dtRange = (start_date, end_date)
            if event["location"] is not None:
                latitude = event["location"]["geo_location"]["latitude"]
                longitude = event["location"]["geo_location"]["longitude"]
                location = {
                    'center': [ latitude, longitude],
                    'bbox': {
                        'latitude' : [ latitude,latitude],
                        'longitude': [ longitude, longitude]
                    }
                }

                contentCluster = []
                additionalInfo = {}
                additionalInfo["UserCount"] = event["participants"]["total"]
                additionalInfo["Category"] = "Unknown"
                additionalInfo["Url"] = event["permalink"]

                Events.append(BasicEvent(event_name, dtRange, location, contentCluster, additionalInfo))
        return Events

    def getAttendingUsersAgesList( self, eventid ):
        print eventid

        guestlist_url = XING_BASE_URL + 'v1/events/' + eventid + '/guests.json?user_fields=birth_date,gender,display_name'
        response = self.client.get(guestlist_url)
        print response.encoding
        jsonResult = json.loads(response.text)

        #print jsonResult
        for currentUserItem in jsonResult["guests"]["items"]:
            user = currentUserItem["user"]
            print "Name: " + user["display_name"] + " Gender: " + user["gender"] + " Birthdate: " + str(user["birth_date"]["day"]) + "-" + str(user["birth_date"]["month"]) + "-" + str(user["birth_date"]["year"]) 
