import sys, os
sys.path.insert(1, os.path.join(sys.path[0], '..'))
sys.path.insert(1, os.path.join(os.path.join(sys.path[0], '..'), '..'))

from FrameworkModels import ClusterFilter, ContentCluster
from inc import helper

class RemoveUnrelatedPhotos(ClusterFilter):
	def filter(self, cluster):
		cluster_features = cluster.AdditionalData

		# calculate per-photo-usage of words
		cluster_words = {}
		for photo in cluster:
			words = photo.Title.split(" ")
			for word in words:
				word = helper.filter_word(word)
				if not(word is None):
					if not(word in cluster_words):
						cluster_words[word] = 0
					cluster_words[word] += 1
		for cluster_word in cluster_words:
			cluster_words[cluster_word] = float(cluster_words[cluster_word]) / float(len(cluster))

		sorted_words = sorted(cluster_words.iteritems(), key=lambda x: -1*x[1]) # sort words according to per-photo-usage in descending order

			
		# filter photos that doesn't contain the most frequently used word
		mostFrequentlyUsedWord = sorted_words[0][0]

		newCluster = []
		newCluster_features = [[] for f in cluster_features]
		for clusterI in range(len(cluster)):
			photo = cluster[clusterI]

			wordsMap = {}
			words = photo.Title.split(" ")
			for word in words:
				word = helper.filter_word(word)
				if not(word is None):
					wordsMap[word] = True

			if mostFrequentlyUsedWord in wordsMap:
				newCluster.append(cluster[clusterI])
				for fI in range(len(cluster_features)):
					newCluster_features[fI].append(cluster_features[fI][clusterI])

		return None if len(newCluster)<=0 else ContentCluster(newCluster, newCluster_features)