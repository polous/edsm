import sys, os
sys.path.insert(1, os.path.join(sys.path[0], '..'))
sys.path.insert(1, os.path.join(os.path.join(sys.path[0], '..'), '..'))

from FrameworkModels import DataFilter
import Levenshtein

class RemoveSimilarsFromSameUser(DataFilter):
	def __init__(self, timespan, levenshtein_ratio):
		self.timespan = timespan
		self.levenshtein_ratio = levenshtein_ratio
		self._data = {}

	def filter(self, photo):
		filter = False
		if photo.Publisher in self._data:
			candidates = []

			ts = ((photo.TimeStamp - self.timespan/2), (photo.TimeStamp + self.timespan/2))
			for d in self._data[photo.Publisher]:
				if d.TimeStamp >= ts[0] and d.TimeStamp <= ts[1]:# and d.GeoLocation.latitude == photo.GeoLocation.latitude	and d.GeoLocation.longitude == photo.GeoLocation.longitude:
					candidates.append(d)

			for c in candidates:
				if Levenshtein.ratio(c.Title, photo.Title) > self.levenshtein_ratio:
					filter = True
					break
			"""
			for d in self._data[photo.Publisher]:
				if d.TimeStamp >= ts[0] and d.TimeStamp <= ts[1]:# and d.GeoLocation.latitude == photo.GeoLocation.latitude	and d.GeoLocation.longitude == photo.GeoLocation.longitude:
					filter = True
					break
			"""
		else:
			self._data[photo.Publisher] = []

		self._data[photo.Publisher].append(photo)
		return filter

	def reset(self):
		self._data = {}
