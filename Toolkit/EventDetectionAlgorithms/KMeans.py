import sys, os
sys.path.insert(1, os.path.join(sys.path[0], '..'))
sys.path.insert(1, os.path.join(os.path.join(sys.path[0], '..'), '..'))

from FrameworkModels import EventDetectionAlgorithm, ContentCluster
import numpy as np
from sklearn.cluster import KMeans
from inc import algos, helper
import sklearn
import sklearn.metrics
import math
import numpy as np

class KMeansAlgorithm(EventDetectionAlgorithm):
	def __init__(self, dt_scale, geo_lat_scale, geo_long_scale, text_dimensions, text_filter, text_scale, text_penalty, num_clusters):
		super(KMeansAlgorithm, self).__init__()
		self.dt_scale = dt_scale
		self.geo_lat_scale = geo_lat_scale
		self.geo_long_scale = geo_long_scale
		self.text_dimensions = text_dimensions
		self.text_filter = text_filter
		self.text_scale = text_scale
		self.text_penalty = text_penalty
		self.num_clusters = num_clusters


	def DetectEvents(self, data, dataProvider):
		# assemble parameters
		FEATURES_DISTANCEMETRIC = PlainSumDistanceMetric
		CLUSTER_METHOD = KMeansClustering(self.num_clusters)
		"""
		OLD (faster, non metric calculating)
		FEATURES = NumericLinearlyScaledCombination((
					(NumericAttribute("TimeStamp"), 60*60*24*2, (CONFIG_DT_START, CONFIG_DT_END)),
						# first metric parameter:		MAXIMAL difference (IN SECONDS) that makes two points BELONG to the same cluster if every other feature matches completely
						# second metric parameter:		data range
					(NumericAttribute(["GeoLocation", "latitude"]), 0.0035, (CONFIG_GEO_BB['latitude'][0], CONFIG_GEO_BB['latitude'][1])),
						# first metric parameter:		MAXIMAL difference (IN DEGREES) that makes two points BELONG to the same cluster if every other feature matches completely
						# second metric parameter:		data range
					(NumericAttribute(["GeoLocation", "longitude"]), 0.0035, (CONFIG_GEO_BB['longitude'][0], CONFIG_GEO_BB['longitude'][1])),
						# first metric parameter:		MAXIMAL difference (IN DEGREES) that makes two points BELONG to the same cluster if every other feature matches completely
						# second metric parameter:		data range
					#(MultiDimNumericTextAttribute("Title", mostFreqUsedWords), 0.01, (0, 1))
				))
		"""
		FEATURES = [
				(NumericAttribute("TimeStamp"), NumericLinearlyScaledSED(self.dt_scale, (helper.datetime_to_ts(dataProvider.DateTimeRange.start), helper.datetime_to_ts(dataProvider.DateTimeRange.end)))),
					# first metric parameter:		MAXIMAL difference (IN SECONDS) that makes two points BELONG to the same cluster if every other feature matches completely
					# second metric parameter:		data range
				(NumericAttribute(["GeoLocation", "latitude"]), NumericLinearlyScaledSED(self.geo_lat_scale, (dataProvider.GeoBoundingBox.latitude[0], dataProvider.GeoBoundingBox.latitude[1]))),
					# first metric parameter:		MAXIMAL difference (IN DEGREES) that makes two points BELONG to the same cluster if every other feature matches completely
					# second metric parameter:		data range
				(NumericAttribute(["GeoLocation", "longitude"]), NumericLinearlyScaledSED(self.geo_long_scale, (dataProvider.GeoBoundingBox.longitude[0], dataProvider.GeoBoundingBox.longitude[1]))),
					# first metric parameter:		MAXIMAL difference (IN DEGREES) that makes two points BELONG to the same cluster if every other feature matches completely
					# second metric parameter:		data range
				(MultiDimNumericTextAttribute("Title", algos.get_mostFrequentlyUsedWords(data, self.text_dimensions, self.text_filter)), MultiDimNumericLinearlyScaledSED2(self.text_scale, self.text_penalty, (0, 1)))
			]


		# assemble feature data
		print "assembling feature data (from {} photos)".format(len(data))
		featureData = None
		featureDistanceMatrices = None
		distanceMatrix = None
		MULTI_FEATURES = type(FEATURES) is list
		if MULTI_FEATURES:
			featureData = []
			featureDistanceMatrices = []

			# extract featureData and calculate feature distance matrices
			for f in FEATURES:
				print "\textracting feature '{}'..".format(str(f[0]))
				featureData.append(f[0].extract_feature(data))
				print "\tcalculating corresponding distance metric '{}'..".format(str(f[1]))
				featureDistanceMatrices.append(f[1].calc_metric(featureData[-1]))

			# calculate global distance matrix
			print "calculating distance matrix"
			distanceMatrix = FEATURES_DISTANCEMETRIC(featureDistanceMatrices)
		else:
			print "\textracting feature '{}'..".format(str(FEATURES))
			featureData = FEATURES.extract_feature(data)


		# extract clusters
		print "extracting clusters"
		if MULTI_FEATURES:
			cluster_indices = CLUSTER_METHOD.extract_clusters_from_distance_matrix(distanceMatrix)
		else:
			cluster_indices = CLUSTER_METHOD.extract_clusters(featureData)


		# assemble clusters
		CLUSTERS = []
		for cind in cluster_indices:
			cluster = [data[i] for i in cind]
			cluster_features = []
			if MULTI_FEATURES:
				for feature in featureData:
					cluster_features.append(feature[cind])
			else:
				cluster_features.append(featureData[cind])

			CLUSTERS.append(ContentCluster(cluster, cluster_features))

		return CLUSTERS

class KMeansClustering(object):
	def __init__(self, num_clusters):
		self.num_clusters = num_clusters 

	def extract_clusters_from_distance_matrix(self, distanceMatrix):
		kmean = KMeans(n_clusters=self.num_clusters).fit(distanceMatrix)
		#dbscan = DBSCAN(eps=self.eps, min_samples=self.min_samples, metric="precomputed").fit(distanceMatrix)
		#dbscan_clusterCount = len(set(dbscan.labels_)) - (1 if -1 in dbscan.labels_ else 0)
		kmean_clusterCount = len(set(kmean.labels_)) - (1 if -1 in kmean.labels_ else 0)
		
		cluster_indices = []
		for i in range(kmean_clusterCount):
			cluster = []
			for j in range(len(kmean.labels_)):
				if int(kmean.labels_[j]) == i:
					cluster.append(j)
			cluster_indices.append(np.asarray(cluster))

		return cluster_indices

	"""
	extract clusters from given data

	params:
		data 	array containing all data points

	returns:
		cluster_indices (array of array of indices describing the clusters), labels (map from data index to cluster label)
	"""
	def extract_clusters(self, data):
		X = np.asarray(data)
		
		### compute clusters (using DBSCAN)
		# compute DBSCAN
		kmean = KMeans(n_clusters=self.num_clusters).fit(X)
		#dbscan = DBSCAN(eps=self.eps, min_samples=self.min_samples, metric="precomputed").fit(distanceMatrix)
		#dbscan_clusterCount = len(set(dbscan.labels_)) - (1 if -1 in dbscan.labels_ else 0)
		kmean_clusterCount = len(set(kmean.labels_)) - (1 if -1 in kmean.labels_ else 0)

		# assemble clusters
		cluster_indices = []
		for i in range(kmean_clusterCount):
			cluster = []
			for j in range(len(kmean.labels_)):
				if int(kmean.labels_[j]) == i:
					cluster.append(j)
			cluster_indices.append(np.asarray(cluster))

		"""
		cluster_labels = {}
		for i in range(data.shape[0]):
			if int(dbscan.labels_[i]) != -1:
				cluster_labels[i] = int(dbscan.labels_[i])
		"""
		return cluster_indices#, cluster_labels





###
###
### FEATURES
###
###
"""
	Extracts features from the given data.
"""
class Feature(object):
	def extract_feature(self, data):
		raise "not implemented"

	def __str__(self):
		return self.__class__.__name__
		
"""
	Extracts a numeric attribute from a set of objects.
"""
class NumericAttribute(Feature):
	def __init__(self, attrName):
		self.attrName = attrName

	"""
		Params:
			data 		list of objects
	"""
	def extract_feature(self, data):
		feature = np.empty((len(data)))
		for i in range(len(data)):
			feature[i] = _getFeature(data[i], self.attrName)
		return feature

	def __str__(self):
		return self.__class__.__name__ + "("+str(self.attrName)+")"

"""
"""
class MultiDimNumericTextAttribute(Feature):
	def __init__(self, attrName, wordFilter):
		self.attrName = attrName
		self.wordFilter = wordFilter

	"""
		Params:
			data 		list of objects
	"""
	def extract_feature(self, data):
		word_dimensions = {}

		dimI = 0
		for datum in data:
			words = _getFeature(datum, self.attrName).split(" ")
			for word in words:
				word = helper.filter_word(word)
				if not(word is None) and not(word in word_dimensions) and (self.wordFilter is None or word in self.wordFilter):
					word_dimensions[word] = dimI
					dimI += 1

		feature = np.zeros((len(data), dimI))
		for datumI in range(len(data)):
			datum = data[datumI]

			words = _getFeature(datum, self.attrName).split(" ")
			for word in words:
				word = helper.filter_word(word)
				if not(word is None) and word in word_dimensions:
					feature[datumI][word_dimensions[word]] = 1

		return feature

	def __str__(self):
		return self.__class__.__name__ + "("+str(self.attrName)+")"

class NumericLinearlyScaledCombination(Feature):
	def __init__(self, feature_defs):
		self.features = []
		self.scales = []
		self.ranges = []

		for feature_def in feature_defs:
			self.features.append(feature_def[0])
			self.scales.append(feature_def[1])
			self.ranges.append(feature_def[2])

	def extract_feature(self, data):
		feature = None
		for i in range(len(self.features)):
			f = self.features[i] 
			s = float(self.scales[i])
			r = self.ranges[i]

			featureData = self._scale_featureData(f.extract_feature(data), s, r)
			if len(featureData.shape) <= 1:
				featureData.shape += (1,)
			if feature is None:
				feature = featureData
			else:
				feature = np.hstack((feature, featureData))
		return feature

	def _scale_featureData(self, featureData, scale, range):
		#if len(featureData.shape) > 1:
		#	scale /= featureData.shape[1]
		return (featureData - range[0]) / scale

def _getFeature(obj, feat):
	if (type(feat) is str) or len(feat) < 2:
		f = feat if type(feat) is str else feat[0]
		return getattr(obj, f)
	else:
		return _getFeature(getattr(obj, feat[0]), feat[1:])





###
###
### FEATURE METRICS
###
###
def PlainSumDistanceMetric(featureDistanceMatrices):
	# simply the squareroot of an equally weighted linear combination of all feature metrics
	# a sum between 0..1 signals that two points should belong to the same cluster
	# a sum >1 signals that two points should not belong to the same cluster
	distanceMetric = np.zeros((featureDistanceMatrices[0].shape)) 
	for fmetric in featureDistanceMatrices:
		distanceMetric = distanceMetric + fmetric
	return distanceMetric#np.sqrt(distanceMetric)





###
###
### CLUSTER METRICS
###
###
"""
	Performs distance metric calculations.
"""
class ClusterMetric(object):
	"""
		Calculates the distance metric for a given set of points.
		An output of 0..1 is supposed to signal, that two points should belong to the same cluster
		An output of >1 is supposed to signal, that two points should not belong to the same cluster
	"""
	def calc_metric(self, data):
		raise "not implemented"

	def __str__(self):
		return self.__class__.__name__
"""
	Scales the numeric input data linearly and calculates the squared euclidean distance.
"""
class NumericLinearlyScaledSED(ClusterMetric):
	def __init__(self, scale, dataRange):
		self.scale = float(scale)
		self.dataRange = dataRange

	"""
		Params:
			data:		numpy.ndarray((n,1)) with n:=number of data points
	"""
	def calc_metric(self, data):
		print "\t\tscaling"
		data_scaled = (data - self.dataRange[0]) / self.scale# / (self.dataRange[1] - self.dataRange[0])
		if len(data_scaled.shape) <= 1:
			data_scaled.shape += (1,)
		print "\t\tcalculating euclidean distance"
		return sklearn.metrics.pairwise.euclidean_distances(data_scaled, data_scaled, squared=True)

"""
	Scales the numeric input data linearly and calculates the squared euclidean distance.
"""
class MultiDimNumericLinearlyScaledSED(ClusterMetric):
	def __init__(self, scale, dataRange):
		self.scale = float(scale)
		self.dataRange = dataRange

	"""
		Params:
			data:		numpy.ndarray((n,m)) with n:=number of data points, m:=number of dimensions
	"""
	def calc_metric(self, data):
		scale_dims = self.scale / data.shape[1]
		data_scaled = (data - self.dataRange[0]) / scale_dims# / (self.dataRange[1] - self.dataRange[0])
		return sklearn.metrics.pairwise.euclidean_distances(data_scaled, data_scaled, squared=True)


"""
	Scales the numeric input data linearly and calculates the squared euclidean distance.
"""
class MultiDimNumericLinearlyScaledSED2(ClusterMetric):
	def __init__(self, scale, penalty, dataRange):
		self.scale = float(scale)
		self.penalty = penalty
		self.dataRange = dataRange

	"""
		Params:
			data:		numpy.ndarray((n,m)) with n:=number of data points, m:=number of dimensions
	"""
	def calc_metric(self, data):
		myMetric = np.zeros((data.shape[0], data.shape[0]))
		inputData = data - self.dataRange[0]
		"""
		for i in range(inputData.shape[0]):
			print str(i)+"/"+str(inputData.shape[0])
			a = inputData[i]
			rowsWithMatches = np.where((a == inputData) & (a != 0))[0]
			#for r in rowsWithMatches:
			#	if myMetric[i][r] == 999999:
			#		myMetric[i][r] = 0
			#	myMetric[i][r] += 1
			myMetric[i][rowsWithMatches] = 0
		"""
		for i in range(inputData.shape[0]):
			#print str(i)+"/"+str(inputData.shape[0])
			myMetric[i] = np.sum((inputData==inputData[i]) & (inputData[i]!=0), axis=1) # number of equal non-zero values

		penalized = myMetric == 0
		nonPenalized = myMetric != 0

		myMetric[penalized] = self.penalty																				# rows with no match -> self.penalty
		myMetric[nonPenalized] = (inputData.shape[1] - myMetric[nonPenalized]) / inputData.shape[1] * self.scale 		# rows with at least one match -> linearly scaled from 0..self.scale
		
		"""
		myMetric = inputData.shape[1] - myMetric
		myMetric = (myMetric / inputData.shape[1]) * self.scale
		myMetric[myMetric == self.scale] = self.penalty
		"""
		"""

		myMetric[penalized] = self.penalty
		myMetric[nonPenalized] = (myMetric[nonPenalized] / inputData.shape[1]) * self.scale
		"""


		"""
		for i in range(inputData.shape[0]):
			for j in range(inputData.shape[0]):
				a = inputData[i]
				b = inputData[j]
				matches = a[(a == b) & (a != 0)].shape[0]

				myMetric[i, j] = (self.scale * (1 - float(matches)/data.shape[1])) if matches>0 else 9999999
		"""

		"""
		scale_dims = self.scale / data.shape[1]
		data_scaled = (data - self.dataRange[0]) / scale_dims# / (self.dataRange[1] - self.dataRange[0])

		sed = sklearn.metrics.pairwise.euclidean_distances(data_scaled, data_scaled, squared=True)
		myMetric = sed
		myMetric[myMetric == data.shape[1]*data_scaled*data_scaled] = 999999
		"""
		return myMetric



