import sys, os
sys.path.insert(1, os.path.join(sys.path[0], '..'))
sys.path.insert(1, os.path.join(os.path.join(sys.path[0], '..'), '..'))

from FrameworkModels import EventFilter

class ContentClusterSizeThreshold(EventFilter):
	def __init__(self, contentClusterSizeThreshold):
		self.threshold = contentClusterSizeThreshold

	def filter(self, event):
		#print "ContentClusterSizeThreshold: ", event.Identifier.encode("utf-8"), ":", len(event.ContentCluster), "<", self.threshold
		return len(event.ContentCluster) < self.threshold