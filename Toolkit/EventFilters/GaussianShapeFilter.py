import sys, os
sys.path.insert(1, os.path.join(sys.path[0], '..'))
sys.path.insert(1, os.path.join(os.path.join(sys.path[0], '..'), '..'))

from FrameworkModels import EventFilter
import numpy as np
from inc import helper 
import math
from statsmodels.sandbox.distributions.extras import mvnormcdf

class GaussianShapeFilter(EventFilter):
	def __init__(self, ratio_threshold, geo_histogram_resolution=(1,1), gauss_scale=10000):
		self.ratio_threshold = ratio_threshold
		self.gauss_scale = gauss_scale
		self.geo_histogram_resolution = geo_histogram_resolution

	def filter(self, event):
		# assemble data (time, geo_lat, geo_long)
		data = np.empty((len(event.ContentCluster), 3))
		for i in range(data.shape[0]):
			data[i][0] = event.ContentCluster[i].TimeStamp
			data[i][1] = event.ContentCluster[i].GeoLocation.latitude
			data[i][2] = event.ContentCluster[i].GeoLocation.longitude
		data_min = np.min(data, axis=0)
		data_max = np.max(data, axis=0)

		histogram_resolution = (int(math.ceil((data_max[0]-data_min[0])/60/60/24)),) + self.geo_histogram_resolution

		# calculate data histogram
		histogram_data, histogram_data_edges = np.histogramdd(data, histogram_resolution)
		histogram_data = histogram_data / data.shape[0]

		# calculate gauss histogram (histogram to be)
		"""
		gauss_mu = np.asarray([
			helper.datetime_to_ts(event.Time["start"] + (event.Time["end"]-event.Time["start"])/2),
			event.Location["center"]["latitude"],
			event.Location["center"]["longitude"],
		])
		gauss_cov = np.asarray([
			[((event.Time["end"]-event.Time["start"])/2).total_seconds(), 0, 0],
			[0, (event.Location["bbox"]["latitude"][1]-event.Location["bbox"]["latitude"][0])/2, 0],
			[0, 0, (event.Location["bbox"]["longitude"][1]-event.Location["bbox"]["longitude"][0])/2],
		])
		"""
		gauss_mu = np.asarray([
			data_min[0] + (data_max[0]-data_min[0])/2,
			data_min[1] + (data_max[1]-data_min[1])/2,
			data_min[2] + (data_max[2]-data_min[2])/2,
		])
		gauss_cov = np.asarray([
			[(data_max[0]-data_min[0])/2 * self.gauss_scale, 0, 0],
			[0, (data_max[1]-data_min[1])/2 * self.gauss_scale, 0],
			[0, 0, (data_max[2]-data_min[2])/2 * self.gauss_scale],
		])

		histogram_gauss = np.zeros(histogram_resolution)
		histogram_gauss_steps = [[(-np.inf, np.inf)]] * 3
		for dimI in range(3):
			if histogram_resolution[dimI] > 1:
				histogram_gauss_steps[dimI] = []
				step = (data_max[dimI]-data_min[dimI]) / histogram_resolution[dimI]
				for i in range(histogram_resolution[dimI]):
					start = data_min[dimI] + step*i
					end = start + step
					histogram_gauss_steps[dimI].append([start, end])
				histogram_gauss_steps[dimI][0][0] = -np.inf
				histogram_gauss_steps[dimI][-1][1] = np.inf

		for timeI in range(len(histogram_gauss_steps[0])):
			timeStart = histogram_gauss_steps[0][timeI][0]
			timeEnd = histogram_gauss_steps[0][timeI][1]
			for geoLatI in range(len(histogram_gauss_steps[1])):
				geoLatStart = histogram_gauss_steps[1][geoLatI][0]
				geoLatEnd = histogram_gauss_steps[1][geoLatI][1]
				for geoLongI in range(len(histogram_gauss_steps[2])):
					geoLongStart = histogram_gauss_steps[2][geoLongI][0]
					geoLongEnd = histogram_gauss_steps[2][geoLongI][1]

					start = np.asarray([timeStart, geoLatStart, geoLongStart])
					end = np.asarray([timeEnd, geoLatEnd, geoLongEnd])
					histogram_gauss[timeI, geoLatI, geoLongI] = mvnormcdf(lower=start, upper=end, mu=gauss_mu, cov=gauss_cov)

		ratio = 1 - np.abs(histogram_data-histogram_gauss).sum() / np.prod(histogram_data.shape)

		#print "GaussianShapeFilter: " + event.Identifier.encode("utf-8") + ": "+ str(ratio)
		return ratio < self.ratio_threshold