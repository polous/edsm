import sys, os
sys.path.insert(1, os.path.join(sys.path[0], '..'))
sys.path.insert(1, os.path.join(os.path.join(sys.path[0], '..'), '..'))

from FrameworkModels import EventInfoExtractor

class ContentPreview(EventInfoExtractor):
	def ExtractInfo(self, data, contentCluster, dataProvider, event, events):
		contentPreview = {"count": len(event.ContentCluster), "content": []}
		for datum in event.ContentCluster:
			contentPreview["content"].append({"title": datum.Title, "url": datum.URL})
		return contentPreview

	def __str__(self):
		return "ContentPreview"