import sys, os
sys.path.insert(1, os.path.join(sys.path[0], '..'))
sys.path.insert(1, os.path.join(os.path.join(sys.path[0], '..'), '..'))

from FrameworkModels import EventInfoExtractor

class OpenStreetMapLink(EventInfoExtractor):
	def ExtractInfo(self, data, contentCluster, dataProvider, event, events):
		return "http://www.openstreetmap.org/?minlon="+str(event.Location["bbox"]["longitude"][0])+"&minlat="+str(event.Location["bbox"]["latitude"][0])+"&maxlon="+str(event.Location["bbox"]["longitude"][1])+"&maxlat="+str(event.Location["bbox"]["latitude"][1])+"&box=yes&mlat="+str(event.Location["center"]["latitude"])+"&mlon="+str(event.Location["center"]["longitude"])

	def __str__(self):
		return "OpenStreetMapLink"