import sys, os
sys.path.insert(1, os.path.join(sys.path[0], '..'))
sys.path.insert(1, os.path.join(os.path.join(sys.path[0], '..'), '..'))

from FrameworkModels import EventInfoExtractor
from inc import algos
import numpy as np

class RelatedItems(EventInfoExtractor):
	def ExtractInfo(self, data, contentCluster, dataProvider, event, events):
		"""
		def extract_fitting_clusters(data, dbscan_params, threshold):
			latestClusterIndices = None
			latestClusterLabels = None
			latestInterClusterVsInnerClusterQuotient = None
			dbscan_eps_counter = 0
			for dbscan_eps in dbscan_params:
				dbscan_eps_counter += 1
				cluster_indices, cluster_labels = extract_clusters(data, dbscan_min_samples=2, dbscan_eps=dbscan_eps)
				interClusterVsInnerClusterQuotient, _, _ = calcInterClusterVsInnerClusterQuotient(data, cluster_indices)
				
				if interClusterVsInnerClusterQuotient >= threshold:
					latestClusterLabels = cluster_labels
					latestClusterIndices = cluster_indices
					break;
				else:
					if latestInterClusterVsInnerClusterQuotient is None or interClusterVsInnerClusterQuotient > latestInterClusterVsInnerClusterQuotient:
						latestClusterLabels = cluster_labels
						latestClusterIndices = cluster_indices
						latestInterClusterVsInnerClusterQuotient = interClusterVsInnerClusterQuotient
					if dbscan_eps_counter >= len(dbscan_params):
						cluster_labels = latestClusterLabels
						cluster_indices = latestClusterIndices
						interClusterVsInnerClusterQuotient = latestInterClusterVsInnerClusterQuotient
						break
					continue

			return cluster_indices, cluster_labels

		# calc related items
		EVENT.AdditionalInfo["related_items"] = []
		related_items_cind, related_items_clabels = extract_fitting_clusters(data_normed[:, 1:3], CLUSTER_PARAMS['eps'], CONFIG_CLUSTER_QUOTIENT_THRESHOLD)
		for rcind in related_items_cind:
			ritem_gauss_means, ritem_gauss_stds = algos.extract_event_gaussians(data[rcind][:, 1:], None)
			
			ritem_latitude_center = data_offsets[1] + ritem_gauss_means[0]*data_scales[1]
			ritem_longitude_center = data_offsets[2] + ritem_gauss_means[1]*data_scales[2]
			ritem_latitude_start = event_latitude_center - (ritem_gauss_stds[0]/2)*data_scales[1]
			ritem_latitude_end = event_latitude_center + (ritem_gauss_stds[0]/2)*data_scales[1]
			ritem_longitude_start = event_longitude_center - (ritem_gauss_stds[1]/2)*data_scales[2]
			ritem_longitude_end = event_longitude_center + (ritem_gauss_stds[1]/2)*data_scales[2]

			EVENT.AdditionalInfo["related_items"].append("http://www.openstreetmap.org/?minlon="+str(ritem_longitude_start)+"&minlat="+str(ritem_latitude_start)+"&maxlon="+str(ritem_longitude_end)+"&maxlat="+str(ritem_latitude_end)+"&box=yes&mlat="+str(ritem_latitude_center)+"&mlon="+str(ritem_longitude_center),)
		"""
		return None

	def __str__(self):
		return "RelatedItems"


def calcInterClusterVsInnerClusterQuotient(data, cluster_indices):
	avgDistance_innerCluster = 0
	avgDistance_interCluster = 0
	clusterI=0
	for cind in cluster_indices:
		clusterI+=1
		cluster = data[cind]

		clusterJ=0
		for cind2 in cluster_indices:
			clusterJ+=1
			cluster2 = data[cind2]

			avgDistance = np.sum(np.sum(sklearn.metrics.pairwise.euclidean_distances(cluster, cluster2)))/(cluster.shape[0]+cluster2.shape[0])
			if clusterJ == clusterI:
				avgDistance_innerCluster += (avgDistance/(cluster.shape[0]+cluster2.shape[0]))
			else:
				avgDistance_interCluster += (avgDistance/(cluster.shape[0]+cluster2.shape[0]))#(avgDistance/len(cluster_indices))
	if len(cluster_indices) > 0:
		avgDistance_innerCluster /= 1.*len(cluster_indices)
	else:
		avgDistance_innerCluster = 0
	if len(cluster_indices) > 1:
		avgDistance_interCluster /= 1.*len(cluster_indices)*(len(cluster_indices)-1)
	else:
		avgDistance_interCluster = 0
	interClusterVsInnerClusterQuotient = avgDistance_interCluster/(avgDistance_innerCluster+avgDistance_interCluster) if avgDistance_interCluster > 0 or avgDistance_innerCluster > 0 else 0
	return interClusterVsInnerClusterQuotient, avgDistance_interCluster, avgDistance_innerCluster