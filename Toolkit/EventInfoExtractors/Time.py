import sys, os
sys.path.insert(1, os.path.join(sys.path[0], '..'))
sys.path.insert(1, os.path.join(os.path.join(sys.path[0], '..'), '..'))

from FrameworkModels import EventTimeExtractor
from inc import algos
import numpy as np
import datetime

class Time(EventTimeExtractor):
	def ExtractInfo(self, data, contentCluster, dataProvider, event, events):
		# fit gaussians
		timeSpaceData = np.empty((len(contentCluster), 1))
		for i in range(len(contentCluster)):
			timeSpaceData[i][0] = contentCluster[i].TimeStamp
		gauss_means, gauss_stds = algos.extract_event_gaussians(timeSpaceData)

		event_start = int(gauss_means[0])-int(gauss_stds[0]/2)
		event_end = int(gauss_means[0])+int(gauss_stds[0]/2)
		
		return (datetime.datetime.fromtimestamp(event_start), datetime.datetime.fromtimestamp(event_end))

	def __str__(self):
		return "Time"