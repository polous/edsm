from FrameworkModels import BasicContent, SocialMediaContentProvider
from inc import helper, algos
from inc.helper import DateTimeRange
import facebook
import sqlite3

from django.utils.dateparse import parse_datetime

APP_ID = '1075761215812214'
APP_SECRET = 'c17a708801cd89c857f1de90618edcfd'
# This new long-lived access token will expire on July 26th, 2017:
ACCESS_TOKEN = "EAAUgz7JURZBwBADiM9eNWz5Tg4E66QMPCzOmGch15KpU8Lmo05cHumIZBLhO0uuHjc8H3746zBdIfOlu2jx0vwqQbdm1P3uihXEx6mT9R0XQEs63usNrQ13KA9MT6TakPE0iVtALrtAigTtzhJ5MRZALP33kDWnZBjcK0oAHZBwZDZD"
URL = "https://facebook.com/events/{event_id}"

class FacebookEvents(BasicContent):
     def __init__(self, dbfacebook):
         super(FacebookEvents, self).__init__(dbfacebook['id'], dbfacebook['description'], dbfacebook['start_time'], helper.GeoLocation(dbfacebook['longitude'], dbfacebook['latitude']), dbfacebook['name'], dbfacebook['url'])

class FacebookContentProvider(SocialMediaContentProvider):
    def __init__(self, dt_range, geo_bb, query, dbfile="../Toolkit/SocialMediaContentProviders/Facebook.sqlite"):
        super(FacebookContentProvider, self).__init__(dt_range, geo_bb)

        self.query = query

        #self.dbcon = sqlite3.connect(dbfile)
        #self.dbcon.row_factory = sqlite3.Row
        #self.dbcon.text_factory = str

    def GetContent(self):
        data = {}

                #            dtRange = DateTimeRange(start_time, end_time)
                #            basicEvent = BasicEvent(event['name'], dtRange, )



                        #basicEvent = BasicEvent(event['name'],  )

                    #{u'attending_count': 1, u'name': u'Jeden Montag ist Hendltag', u'start_time': u'2013-02-25T10:00:00+0100', u'place': {u'id': u'119398204757022', u'name': u'Augustiner Br\xe4ustuben', u'location': {u'city': u'Munich', u'zip': u'80339', u'country': u'Germany', u'longitude': 11.54581855443, u'street': u'Landsberger Stra\xdfe', u'latitude': 48.139389315007}}, u'end_time': u'2013-05-27T16:00:00+0200', u'id': u'473646382685155'}
                #for event in idDict['data']:
                #    print type(event).__name__


            #for event in eventsDict['events']:
            #    print events



        #filter events for query, start date and end date


        #for event in eventDict:
        #    print event
        #print dict.items(results)

        #return events
        #   "description": "Uhrzeit tbc",
        #   "end_time": "2016-12-02T08:00:00+0100",
        #   "name": "Tollwood",
        #   "place": {
        #     "name": "Wintertollwood Theresienwiese",
        #     "location": {
        #       "city": "Munich",
        #       "country": "Germany",
        #       "latitude": 48.133733530219,
        #       "longitude": 11.550473005232,
        #       "zip": "80336"
        #     },
        #     "id": "205481262922205"
        #   },
        #   "start_time": "2016-12-02T06:00:00+0100",
        #   "id": "632960300196675"
        # }




#print results['data'][0]['description']
#print results['data'][0]
