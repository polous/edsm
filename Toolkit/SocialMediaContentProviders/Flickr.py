import sys, os
sys.path.insert(1, os.path.join(sys.path[0], '..'))
sys.path.insert(1, os.path.join(os.path.join(sys.path[0], '..'), '..'))
from inc import helper, word_filters
from FrameworkModels import BasicContent, SocialMediaContentProvider
import sqlite3
from dateutil.relativedelta import relativedelta
import time


API_KEY = '06cbcf591f573aeed851a3ca07508169'
API_SECRET = 'a28c0b5be200a1b9'
PHOTO_URL = "https://www.flickr.com/photos/{user}/{photo_id}"

class FlickrPhoto(BasicContent):
	def __init__(self, dbphoto, flickr_timeattr="datetime_taken"):
		super(FlickrPhoto, self).__init__(dbphoto['id'], dbphoto['title'], dbphoto[str(flickr_timeattr)], helper.GeoLocation(dbphoto['geo_latitude'], dbphoto['geo_longitude']), dbphoto['owner'], dbphoto['url'])

class FlickrContentProvider(SocialMediaContentProvider):
	def __init__(self, dt_range, geo_bb, flickr_timeattr="datetime_taken", dbfile="../Toolkit/SocialMediaContentProviders/Flickr.sqlite"):
		super(FlickrContentProvider, self).__init__(dt_range, geo_bb)

		self.flickr_timeattr = flickr_timeattr

		self.dbcon = sqlite3.connect(dbfile)
		self.dbcon.row_factory = sqlite3.Row


	def GetContent(self):
		data = []
		
		self._load_data()

		dbres = self.dbcon.execute("\
			SELECT\
				id, owner, title, datetime_taken, datetime_posted, geo_latitude, geo_longitude, url\
			FROM\
				flickr_photos\
			WHERE\
				{0}>=? AND {0}<=? AND geo_latitude>=? AND geo_latitude<=? AND geo_longitude>=? AND geo_longitude<=?\
			".format(self.flickr_timeattr),

			(helper.datetime_to_ts(self.DateTimeRange.start), helper.datetime_to_ts(self.DateTimeRange.end), self.GeoBoundingBox.latitude[0], self.GeoBoundingBox.latitude[1], self.GeoBoundingBox.longitude[0], self.GeoBoundingBox.longitude[1])
		)

		for row in dbres:
			data.append(FlickrPhoto(row, self.flickr_timeattr))

		return data


	def _load_data(self):
		dbres = self.dbcon.execute("\
			SELECT\
				COUNT(*) AS `count`\
			FROM\
				flickr_photos_index\
			WHERE\
				{0}_low IS NOT NULL AND {0}_low<=?\
				AND {0}_high IS NOT NULL AND {0}_high>=?\
				AND geo_latitude_low<=?\
				AND geo_latitude_high>=?\
				AND geo_longitude_low<=?\
				AND geo_longitude_high>=?\
			".format(self.flickr_timeattr),

			(helper.datetime_to_ts(self.DateTimeRange.start), helper.datetime_to_ts(self.DateTimeRange.end), self.GeoBoundingBox.latitude[0], self.GeoBoundingBox.latitude[1], self.GeoBoundingBox.longitude[0], self.GeoBoundingBox.longitude[1])
		).fetchone()
		if (dbres is None) or (dbres["count"] <= 0):
			print "downloading data.."

			## INITIALIZE FLICKR API ##
			import flickrapi

			flickr = flickrapi.FlickrAPI(API_KEY, API_SECRET)

			#(token, frob) = flickr.get_token_part_one(perms='read') # OAuth not needed since we access the API read-only 
			#if not token: raw_input("Press ENTER after you authorized this program")
			#flickr.get_token_part_two((token, frob))
			###########################
			
			##### DOWNLOAD PHOTOS #####
			# download each day separately (-> dl in chunks)
			chunks = (self.DateTimeRange.end-self.DateTimeRange.start).days+1
			for i in range(chunks):
				# assemble flickr arguments
				farg_timestart = self.DateTimeRange.start + relativedelta(days=+i)
				farg_timeend   = farg_timestart + relativedelta(days=+1) + relativedelta(seconds=-1)
				farg_geobbox = (self.GeoBoundingBox.longitude[0], self.GeoBoundingBox.latitude[0], self.GeoBoundingBox.longitude[1], self.GeoBoundingBox.latitude[1])

				print "\tprocessing.. " + farg_timestart.strftime("%Y-%m-%d")

				# download photos page wise
				currentPage = 1
				lastPage = 1
				while currentPage <= lastPage:
					print "min_upload_date", time.mktime(farg_timestart.timetuple()), "max_upload_date", time.mktime(farg_timeend.timetuple()), "bbox",farg_geobbox, "extras","date_upload,date_taken,geo", "per_page",100, "page",currentPage		
					res = flickr.photos_search(min_upload_date=time.mktime(farg_timestart.timetuple()), max_upload_date=time.mktime(farg_timeend.timetuple()), bbox=(str(farg_geobbox[0])+","+str(farg_geobbox[1])+","+str(farg_geobbox[2])+","+str(farg_geobbox[3])), extras="date_upload,date_taken,geo", per_page=100, page=currentPage)
					pcount = res.find("photos").attrib['total']
					print "\t\t" + res.find("photos").attrib["page"] + ": " + str(currentPage*int(res.find("photos").attrib["perpage"])) + "/" + res.find("photos").attrib["total"]

					for photo in res.find("photos").findall("photo"):
						try:
							url = PHOTO_URL.format(user=str(photo.attrib["owner"]), photo_id=str(photo.attrib["id"]))
							self.dbcon.execute('DELETE FROM flickr_photos WHERE id=?', (int(photo.attrib["id"]),))
							self.dbcon.execute('INSERT INTO flickr_photos(id, owner, title, datetime_taken, datetime_posted, geo_latitude, geo_longitude, url) VALUES(?,?,?,?,?,?,?,?)', (int(photo.attrib["id"]), photo.attrib["owner"], photo.attrib["title"], helper.time_str_to_float(photo.attrib["datetaken"], "%Y-%m-%d %H:%M:%S"), int(photo.attrib["dateupload"]), float(photo.attrib["latitude"]), float(photo.attrib["longitude"]), url))
						except:
							import traceback
							print "insert failed:", traceback.format_exc()
							pass
					self.dbcon.commit()

					currentPage += 1
					lastPage = int(res.find("photos").attrib["pages"])
			###########################

			###### ADD TO INDEX #######
			# TODO: once a day is added to the index it won't get queried again => if the current day is added no future entries for this day will be recorded
			self.dbcon.execute("\
				INSERT INTO\
					flickr_photos_index({0}_low, {0}_high, geo_latitude_low, geo_latitude_high, geo_longitude_low, geo_longitude_high)\
				VALUES\
					(?,?,?,?,?,?)\
				".format(self.flickr_timeattr),

				(helper.datetime_to_ts(self.DateTimeRange.start), helper.datetime_to_ts(self.DateTimeRange.end), self.GeoBoundingBox.latitude[0], self.GeoBoundingBox.latitude[1], self.GeoBoundingBox.longitude[0], self.GeoBoundingBox.longitude[1])
			)
			self.dbcon.commit()
			###########################
			
			print "\n"
