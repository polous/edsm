# -*- encoding: utf-8-*-

'''
This script saves daily tweets in a sqlite database file
'''
import sys
sys.path.append('/Users/dominik/Development/IDP/django_project/edsm')

from Toolkit.SocialMediaContentProviders.Twitter import TwitterContentProvider
from datetime import date, timedelta, datetime
from inc.helper import DateTimeRange
from inc.helper import GeoBoundingBox

import locale
locale.setlocale(locale.LC_ALL, 'de_DE.UTF-8')

#time
end_dt = datetime.now()
start_dt = end_dt - timedelta(1)
timeRange = DateTimeRange(start_dt, end_dt)

#database file
db_file ="test.sqlite"


# geo bounding box
minLat = 47.923046
minLon = 11.199888
maxLat = 48.427553
maxLon = 12.030220
geo_bb = GeoBoundingBox(minLat, minLon, maxLat, maxLon )


queries = ["München", "münchen", "Munich", "munich", "Tollwood", "tollwood"]

for query in queries:
    print query
    provider = TwitterContentProvider( timeRange, geo_bb, query, db_file )
    provider._load_data()
