from __future__ import unicode_literals

from django.apps import AppConfig


class EventvisualizerConfig(AppConfig):
    name = 'eventvisualizer'
