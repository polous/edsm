# -*- encoding: utf-8-*-
from django import forms
#from leaflet.forms.widgets import LeafletWidget
from datetime import date, timedelta, datetime
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm


class FullUserCreationForm(UserCreationForm):
    """
    """
 
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ['email', 'username', 'first_name', 'last_name', "password1", "password2" ]
        #fields = ("username", "email", )

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
        return user

class EventSearchForm(forms.Form):
    """
    """
 
    event_name = forms.CharField(label='Search query', max_length=100, initial="M")
    start_date = forms.DateField(initial=datetime.now() - timedelta(7))
    end_date = forms.DateField(initial=datetime.now())
    social_media = forms.CharField(widget = forms.HiddenInput(), required = False)
    minLat = forms.FloatField( widget = forms.HiddenInput(), required = True)
    maxLat = forms.FloatField( widget = forms.HiddenInput(), required = True)
    minLon = forms.FloatField(widget = forms.HiddenInput(), required = True)
    maxLon = forms.FloatField( widget = forms.HiddenInput(), required = True)

class EventSaveForm(forms.Form):
    """
    """
 
    ev_name = forms.CharField(label='Name', max_length=100)
    #ev_category = forms.ChoiceField(label='Category', choices=CATEGORIES)
    ev_category = forms.ChoiceField(label='Category', widget=forms.Select(attrs={"onChange":'dropdown_handler(this.value)'}) )
    ev_subcategory = forms.ChoiceField(label='Subcategory')
    
