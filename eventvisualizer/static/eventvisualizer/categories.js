
/**
 * category functions.

 * @author Astrid Berchtold <berchtoa@in.tum.de>
 * @author Katharina Bui <buik@in.tum.de>
 * @author Dominik Drexler <drexledo@in.tum.de>
 */

// The map for the categories and the subcategories
// Map structur: [ category : "category_name"m, subcategories : [list of subcategories] ]
var db_categories = [
  { category : "Concert", subcategories :
        ["50s/60s era", "Alternative", "Bluegrass", "Children/Family", "Classical", "Comedy", 
        "Country/Folk", "Festival Tour", "Hard Rock/Metal", "Holiday", "Jazz/Blues",
        "Las Vegas Shows", "Latin", "New Age", "Pop/Rock", "RnB/Soul", "Rap/Hip Hop",
        "Reggae/Reggaeton", "Religious", "Techno/Electronic", "World", "Other"]},
  { category : "Natural", subcategories : ["Avalanche", "Blizzard", "Cyclonic storm", "Drought", "Earthquake", "Flood", "Hailstorm", "Heat wave",
  "Hurricane", "Landslide", "Limnic eruptions", "Tornadoe", "Tsunami", "Volcanic eruption", "Wildfire", "Other" ]},
  { category : "Political", subcategories : ["Conferences", "Debate", "Election", "Party", "Protest",
  "Meeting", "Rally", "Riot", "Seminar", "Other"]},
  { category : "Social", subcategories : ["Art", "Carnival", "Ceremonies", "Circus", "Costume", "Fairs/Festivals",
  "Film", "Food/wine", "Meeting", "Networking", "Open Day", "Open Music",
  "Outdoor", "Parade", "Party", "Show", "Magic Shows", "Religious", 
  "Unconferences", "Other"]},
  { category : "Sport", subcategories : ["Archery", "Athletics", "Badminton", "Basketball", "Boxing", "Canoe kayak Slalom", "Canoe kayak Sprint", "Cycling BMX",
  "Cycling Road", "Cycling Track", "Mountain Bike", "Diving", "Swimming", "Synchronized Swimming", "Water polo",
  "Equestrian Dressage", "Equestrian Eventing", "Equestrian Jumping", "Fencing", "Football", "Golf", "Artistic Gymnastics",
  "Rhythmic Gymnastics", "Trampoline", "Handball", "Hockey", "Judo", "Modern pentathlon", "Rowing", "Rugby", "Sailing", "Shooting",
  "Table tennis", "Taekwondo", "Tennis", "Triathlon", "Beach volleyball", "Volleyball", "Weightlifting", "Greco-Roman Wrestling",
  "Freestyle Wrestling", "Biathlon", "Bobsleigh", "Bobsleigh Skeleton", "Curling", "Ice Hockey", "Luge", "Figure skating",
  "Short Track Speed Skating", "Speed Skating", "Alpine Skiing", "Cross Country Skiing", "Freestyle Skiing",
  "Nordic Combined Skiing", "Ski Jumping", "Snowboard", "Other"]},
  { category : "Traffic", subcategories : ["Closure", "Pattern", "Other"]},
  { category : "Accident", subcategories : [ "Animal Accidents", "Animal Bite", "Assaults", "Bicycle", "Boat/Ship", "Bus", "Car", 
 	"Clinical Negligence", "Criminal", "Factory", "Falls", "Food Poisoning",  
  "Horse", "Industrial", "Medical Negligence", "Plane", "Public Liability",
 	"Slips/Trips", "Snow", "Sports", "Taxi", "Train", "Tram", "Water", "Work", "Other"]},
  { category : "Construction", subcategories : ["Apartments", "Condominiums", "Cottages", "Houses", "Residential", "Single Unit Dwellings", "Townhouses",  
 	"Clinics", "Commercial", "Hotels", "Hospitals", "Schools", "Institutional", "Light Manufacturing Plants",
 	"Retail Chain Stores", "Shopping Centres", "Sports Facilities", "Stadiums", "Universities", "Warehouses", "Skyscrapers", 
 	"Chemical Processing Plants", "Industrial", "Manufacturing", "Medicine", "Nuclear power plants",
 	"Power Generation", "Petroleum", "Steel Mills", "Oil Refineries", 
 	"Highway", "Roads", "Stations", "Streets", "Alleys", "Runways", "Paths", "Parking",
 	"Water", "Sewer Line", "Dams", "Sewage Treatment Plants/Facilities", "Flood Control", 	 	 	 
 	"Dredging", "Water Treatment Plants/Facilities"]},
  { category : "Exhibition", subcategories : ["Air", "Art", "Auto", "Boat", "Book", "Children", "Cloth", "Computer", "Food",
  "Horticultural", "Motorcycle", "Oil", "Robotic", "Science", "Sport", 
 	"Technology", "Travelling", "Other"]},
  { category : "Educational", subcategories : ["Debates", "Lectures", "Philosophical Lectures", "Symposia", "Other"]},
  { category : "Conference", subcategories : ["Academic", "Businees", "Cultural", "Diplomatic", "Environmental", "International", 	 
 	"Legal", "Medical", "Peace", "Political", "Science Fiction", "Technology", "Youth", "Other"]},
   { category : "Other", subcategories : []}
]

// List of facebook categories
var facebook_categories = [
  "ART_EVENT", "BOOK_EVENT", "MOVIE_EVENT", "FUNDRAISER", "VOLUNTEERING", "FAMILY_EVENT", "FESTIVAL_EVENT", "NEIGHBORHOOD",
  "RELIGIOUS_EVENT", "SHOPPING", "COMEDY_EVENT", "MUSIC_EVENT", "DANCE_EVENT", "NIGHTLIFE", "THEATER_EVENT",
  "DINING_EVENT", "FOOD_TASTING", "CONFERENCE_EVENT", "MEETUP", "CLASS_EVENT", "LECTURE", "WORKSHOP", "FITNESS", "SPORTS_EVENT", 
  "OTHER", "UNKNOWN"
]

// The map for the colors
// Colors can be changed here
var colorMap = {
  "concert": "darkred", 
  "political": "darkpurple", 
  "sport": "darkgreen",
  "natural": "lightgreen", 
  "social": "blue", 
  "traffic": "purple",
  "accident": "red", 
  "construction": "darkblue",
  "exhibition": "orange",
  "educational": "green", 
  "conference": "lightred",
  "other": "cadetblue",
  "facebook_default":  "cadetblue",
  "flickr_default": "blue",
  "xing_default": "cadetblue",
  "twitter_default": "lightblue"
}

var colorNameToHex = {
  "darkred": "#A23037",
  "darkpurple": "#5B3967",
  "darkgreen": "#718136",
  "lightgreen": "#B7F884",
  "blue": "#29ABD8",
  "purple": "#9B3C82",
  "red": "#DA3B31",
  "darkblue": "#00689E",
  "orange": "#FA9546",
  "green": "#6AAA41",
  "lightred": "#FF8D82",
  "cadetblue": "cadetblue",
  "lightblue": "lightblue",
  "flickr_blue": "#0063DC"
}


/**
 *  Method which gets called if site has loaded completly. 
 * It creates all category elements whit corresponding subcategories
 * This is needed for the dropdown menues
 * 
 */
$(document).ready(function(){
  var catSelect = document.getElementById("id_category");

    var select = document.getElementById("id_ev_category");

    if( !select && !catSelect) {
      return;
    }

    for(var i = 0; i < db_categories.length ; ++i) {
      if( db_categories[i].category == "Other") {
        continue;
      }

        var option = document.createElement('option');
        option.text = option.value = db_categories[i].category;

        if(select) {
            select.add(option, 0);
        }

        if(catSelect) {
            catSelect.add(option,0);
    }

    // set default value

    if(select) {
    setSubcategoriesForCurrentValue();
    }
    }
         // add option all
     if(catSelect) {
       var option = document.createElement('option');
      option.text = "All";
      option.value = "";
      catSelect.add(option,0);
      option.selected = true;
     }
});

/**
 * Set the subcategories for a given category.
 * It also updates the dropdown list of the subcategories
 * 
 */
function setSubcategoriesForCurrentValue() {
  var cat_select = document.getElementById("id_ev_category");
  var cat_name = cat_select.value;

  var subcats = getSubcategories(cat_name);
  setSubcategories(subcats);
}

/**
 *  Get the subcategories of a category
 * 
 * @param category_name String of the category name
 */
function getSubcategories( category_name ) {
  for( var i = 0; i < db_categories.length; ++i ) {
    if( db_categories[i].category == category_name) {
      return db_categories[i].subcategories;
    }
  }
}

/**
 * Set the subcategories in the dropdown menue
 * 
 * @param list of subcategories
 */

function setSubcategories( subcategories ) {
  //var select =   $("#ev_subcategories");
  $("#id_ev_subcategory").empty()
  var select = document.getElementById("id_ev_subcategory");
  //select.empty();
  for(var i = 0; i < subcategories.length ; ++i) {
          if( subcategories[i] == "Other") {
              continue;
      }
      var option = document.createElement('option');
      option.text = option.value = subcategories[i];
      select.add(option, 0);
  }
}

/**
 * Handler for the category dropdown 
 * 
 * @param value Current value of the category dropdown
 */
function dropdown_handler(value) {
  var subcats = getSubcategories(value);
  setSubcategories(subcats);
}

/**
 *  Map facebook category to database category
 * 
 * @param fb_category String the facebook category String
 * @return String the database category
 * 
 */
function convertFbCategoryToDBCategory( fb_category) {
  var db_category = "";

  switch(fb_category) {
    case "MUSIC_EVENT":
      db_category = "Concert";
      break;
    case "MEETUP":
      db_category = "Political";
      break;
    case "ART_EVENT":
    case "MOVIE_EVENT":
    case "FUNDRAISER":
    case "VOLUNTEERING":
    case "FAMILY_EVENT":
    case "NEIGHBORHOOD":
    case "RELIGIOUS_EVENT":
    case "SHOPPING":
    case "COMEDY_EVENT":
    case "DANCE_EVENT":
    case "NIGHTLIFE":
    case "THEATER_EVENT":
    case "DINING_EVENT":
    case "FOOD_TASTING":
    case "FESTIVAL_EVENT":
    case "WORKSHOP":
      db_category = "Social";
      break;
    case "FITNESS":
    case "SPORTS_EVENT":
      db_category = "Sport";
      break;
    case "BOOK_EVENT":
      db_category = "Exhibitions";
      break;
    case "CLASS_EVENT":
    case "LECTURE":
      db_category = "Educational";
      break;
    case "CONFERENCE_EVENT":
      db_category = "Conference";
      break;
    default:
      db_category = "Other";
      break;
  }

  return db_category;

}

/**
 *  Get the color of a social media type
 *  
 * @param category Current category String
 * @param social_media The social media type String
 * @returns color for the category
 */
function getCategoryColor(category, social_media) {

  if(typeof category === "undefined" || category === "") {
    switch(social_media) {
      case "Twitter":
        category = "twitter_default";
        break;
      case "Flickr":
        category = "flickr_default";
        break;
      case "Facebook":
        category = "facebook_default";
        break;
      case "Xing":
        category = "xing_default";
        break;
      default:
        category = "other";
    }
  }

  var color = colorMap[category.toLowerCase()];


// if category has no color, use color of category "other"
  if(typeof color === "undefined" || color === "") {
    color = colorMap["other"];
  }

  return color;
}

/**
 *  Get the category icon
 * 
 * @param category The category String
 * @param social_media The social media type String
 * @param iconname String of the icon name, default = ""
 * @returns The category icon 
 * 
 */
function getCategoryIcon(category, social_media, iconname = "") {

  var color = getCategoryColor(category, social_media);

  var fa_icon;
  switch(social_media) {
    case "Facebook":
      fa_icon = "facebook";
      break;
    case "Twitter":
      fa_icon = "twitter";

      break;
    case "Flickr":
      fa_icon = "flickr";
      break;
    case "Instagram":
      fa_icon = "instagram";
      break;
    case "Xing": 
       fa_icon = "xing";
      break;
    case "Database":
      //fa_icon = "database";
      fa_icon = iconname.toLocaleLowerCase();
      break;
  }


  return L.AwesomeMarkers.icon({
    icon: fa_icon,
    prefix: "fa",
    markerColor: color
  });
}

/**
 *  Generate the legend for the categories and add it to the map
 * 
 */
function generateLegend() {

var legendString = '<table style="width:100%;height:200px;width:200px;font-size:20px;">';
for( var i = 0; i < db_categories.length; ++i ) {
    var cat = db_categories[i].category;
    var color =  getCategoryColor(db_categories[i].category, "Database");

    if(cat == "Other") {
      continue;
    }

    color = colorNameToHex[color];

    var icon_string = '<div style="width:20px;height:20px;background-color:' + 
                      color + ';"></div>'; 
    //var test = getCategoryIcon(db_categories[i].category).createIcon().outerHTML;
    legendString += "<tr>"
    legendString += "<td>" + db_categories[i].category +"</td>";
    legendString += "<td>" + icon_string + "  </td>";
    legendString += "</tr>";
  }

  legendString += "</table>";
  document.getElementById("legend").innerHTML = legendString;  
}
