/**
 * Document initialization functions.

 * @author Astrid Berchtold <berchtoa@in.tum.de>
 * @author Katharina Bui <buik@in.tum.de>
 * @author Dominik Drexler <drexledo@in.tum.de>
 */


var howoften= [
	"unknown",
	"once",
	"dialy",
 	"weekly",
 	"monthly",
	"yearly"

]


/**
 * Function which is called on startup
 * 
 * It hides the save form, the loading sign and the save button
 * 
 */
$(document).ready(function(){

  if($('#save_form') != "") {
    $('#save_form').hide();
  }

  if ($('#loading') != "") {
        $('#loading').hide();
    }
	
	  if ($('#loadingDiv') != "") {
        $('#loadingDiv').hide();
    }


  var element = document.getElementById("show_save_form");

  if( element) {
    //$('#show_save_form').hide();

    element.onclick = function() {
      $('#save_form').show();
    };
  };

  element = document.getElementById("save_event");
  if( element) {
    element.onclick = function() {
      sendEventToServer();
    };
  };



 element = document.getElementById("id_ev_howoften");
 if(element) {
	 for(var i = 0; i < howoften.length ; ++i) {
		 var option = document.createElement('option');
		option.text = option.value = howoften[i];
            element.add(option, 0);
}


}

});


