/**
 * Helper functions.

 * @author Astrid Berchtold <berchtoa@in.tum.de>
 * @author Katharina Bui <buik@in.tum.de>
 * @author Dominik Drexler <drexledo@in.tum.de>
 */


/**
 * This method sends a post request
 * 
 *
 * @param url_string The URL where the request should be send to
 * @param data The data in json format which should be send
 * @param successCallback Callback function if request gives successfull response
 * @param errorCallback Callback function if error occures
 */

// This method sends post request to the specified url with the data
function sendPostRequest(url_string, data, successCallback, errorCallback) {
  $.ajax({
    type: "POST",
    url: url_string,
    data: data,
    success: function (response) {
      // For example, filter the response
      successCallback(response);
    },
    error: function (xhr, errmsg, err) {
      errorCallback(xhr, errmsg, err);
    }
  })
}

/**
 * This method sends a get request
 * 
 *
 * @param url_string The URL where the request should be send to
 * @param data The data in json format which should be send
 * @param successCallback Callback function if request gives successfull response
 * @param errorCallback Callback function if error occures
 */

function sendGetRequest(url_string, data, successCallback, errorCallback) {
  $.ajax({
    type: "GET",
    url: url_string,
    data: data,
    success: function (response) {
      // For example, filter the response
      successCallback(response);
    },
    error: function (xhr, errmsg, err) {
      errorCallback(xhr, errmsg, err);
    }

  });
}

/**
 * This method extends the array class for a function "contains"
 * 
 * @param item Item which should be searched inside the array
 * @return true if item is in array, false if not
 */
Array.prototype.contains = function(item) { 
  var idx = this.length; 

  while (idx--) { 
    if (this[idx] === item) { 
      return true;
    } 
  } 
  return false; 
};



/**
 *  Decode html to fit to server text coding
 * 
 * @param original html text
 * @returns text in server coding
 */
function decodeHtml(html) {
  var txt = document.createElement("textarea");
  txt.innerHTML = html;
  return txt.value;
}

/**
 * Capitalize the first letter of a string
 * 
 * @param string The string
 * @return string The same string but with capitalized first letter
 */
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}


/**
 * Check if string is empty
 * 
 * @param str String which should be checked
 * @return true if empty, false if not
 */
function isEmpty(str) {
    return (!str || 0 === str.length);
}

/**
 * Convert the date string to a specific representation DD/MM/YYYY
 * 
 * @param date_string The input date as string
 * @return date as String DD/MM/YYYY
 */
function convertDateString(date_string) {
  var date = new Date(date_string);
  return (date.getDate() + '/' + (date.getMonth() + 1) + '/' +  date.getFullYear());
}

/**
 * Check the location information from nominatim on a specific location
 * 
 * @param lat Latitude of the location
 * @param lon Longitude of the location
 * @return Location information string
 * 
 */
function getLocationInfo(lat, lon) {
  var locationString = "";

  $.ajax({
      type: "GET",
      url: "https://nominatim.openstreetmap.org/reverse.php",
      data: { "format" : "json",
        "lat": lat, "lon" : lon, "addressdetails": 1},
      async: false,
      success: function (response) {
        locationString = convertLocationInfo(response);
            },
      error: function (xhr, errmsg, err) {
      }
  }); 
  return locationString;
}

/**
 * Converts the location information from nominatim to a string
 * It filters out some necessary information and leaves out the not so important ones
 * 
 * @param locationInfo Location information from nominatim
 * @return Location information as string
 * 
 */
function convertLocationInfo(locationInfo) {
  var curAdress = locationInfo["address"];

  var excludeList = [ "suburb", "state", "house_number","neighbourhood", "city_district", "state_district", "postcode", "country_code", "country"]

  var locInfo = "";
  for( var key in curAdress) {
    if( excludeList.contains(key) ) {
      continue;
    }

    if( locInfo != "" ) {
      locInfo += ", ";
    }
    locInfo += curAdress[key];
  }

  return locInfo;
}

/**
 * Converts the umlauts in a string to ue/ae/oe/ss
 * 
 * @param string
 * @return converted string
 * 
 */
function convertUmlaut(str) {
	str = str.replace(/\u00c4/g, "Ae");
	str = str.replace(/\u00e4/g, "ae");
	str = str.replace(/\u00d6/g, "Oe");
	str = str.replace(/\u00f6/g, "oe");
	str = str.replace(/\u00dc/g, "Ue");
	str = str.replace(/\u00fc/g, "ue");
	str = str.replace(/\u00df/g, "ss");
	
	return str;
}

// Ü, ü     \u00dc, \u00fc
// Ä, ä     \u00c4, \u00e4
// Ö, ö     \u00d6, \u00f6
// ß        \u00df
