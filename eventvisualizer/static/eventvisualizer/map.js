/**
 * Map Frontend javascript for openeventmap application.

 * @author Astrid Berchtold <berchtoa@in.tum.de>
 * @author Katharina Bui <buik@in.tum.de>
 * @author Dominik Drexler <drexledo@in.tum.de>
 */

// maximum amount social media information 
var maxDisplayInfo = 20;

// current index in marker list
var currentMarkerIndex = 0;

// the leaflet osm map
var eventmap;

// the clicked event
var currentEvent;

// the last searched social media
var currSocialMedia;

// The type of the webpage
// can be New_Events or something else
var currentType;

// The currently displayed events
// Can be empty if no events are displayed
var currentEvents;

// marker map
var markerMap;

// The array of all markers displayed on the map
var currentMarkers = new Array();

// the marker cluster
var markerCluster = L.markerClusterGroup({
  iconCreateFunction: ownIconCreateFunction
});

// the heatmap layer
var heatmapLayer =  L.heatLayer([]);

// array of arrays
var eventsMarkerLists = new Array();

// The legend
var Legend;

/**
 * Basic class for events in javascript
 */

class Event {
  constructor(Title, ContentCluster) {
    this.Title = Title;
    this.ContentCluster = ContentCluster;
  }
}

/**
 * Basic class for the event marker
 * 
 * This class stores every information which is needed for the markers
 * 
 */

class EventMarker {
  constructor(Title, StartDate, EndDate, Lat, Lon, ContentCluster, AdditionalInfo, Popup, Icon) {
    this.Title = Title;
    this.StartDate = StartDate;
    this.EndDate = EndDate;
    this.Lat = Lat;
    this.Lon = Lon;
    this.ContentCluster = ContentCluster;
    this.AdditionalInfo = AdditionalInfo;
    this.Popup = Popup;
    this.Icon = Icon;
    this.LocationText = "";
  }

  /**
   * 
   * Getter method for getting the category of the event 
   * 
   */
  getCategory() {
    return this.AdditionalInfo["category"];
  }

    /**
   * 
   * Getter method for getting the subcategory of the event 
   * 
   */
  getSubcategory() {
    return this.AdditionalInfo["subcategory"];
  }

  /**
   * 
   * Getter method for getting the comments of the event 
   * 
   */
  getComments() {
    return this.AdditionalInfo["comments"];
  }

  /**
   * 
   * Convert to Json
   * 
   */
  toJsonData() {

  
   var contentClusterData = []

  for( var counter = 0; counter < this.ContentCluster.length; ++counter) {
       var content = this.ContentCluster[counter];
       var contentJson = {
         "Publisher" : content["Publisher"],
         "Title" : content["Title"],
         "URL"  : content["URL"],
         "TimeStamp" : content["TimeStamp"],
         "Longitude" : content["Longitude"],
         "Latitude" : content["Latitude"]        
       }

       contentClusterData.push(contentJson);
     }

     
     return {
     'Title' : this.Title,
     "StartDate" : this.StartDate,
     "EndDate" : this.EndDate,
     "Lat" : this.Lat,
     "Lon" : this.Lon,
     "ContentCluster" : contentClusterData,
     "AdditionalInfo" : this.AdditionalInfo
   }
  }
}

/**
 * Basic class for the location
 * 
 * This class stores the latitude and longitude of a location
 * 
 */
class Location {
  constructor(Latitude, Longitude) {
    this.Latitude = Latitude;
    this.Longitude = Longitude;
  }
}


/**
 * Send the currently selected event to the server
 * 
 * This is used to store the event to the database.
 * It gathers all needed information form the clicked event and the
 * entered values like name, category and subcategory and 
 * send them with a post request to the server.
 * 
 */

// Send the name, the category and the social media to the server
// The server send back the information to this event which is stored inside the database
function sendEventToServer() {
  var ev_name = document.getElementById("id_ev_name").value;
  var comment = document.getElementById("id_ev_comment").value;
  var ev_cat = document.getElementById("id_ev_category").value;
  var ev_subcat = document.getElementById("id_ev_subcategory").value;
  var ev_how_often = document.getElementById('id_ev_howoften').value;
  var start_date = currentEvent.StartDate;
  var end_date = currentEvent.EndDate;
  var lat = currentEvent.Lat;
  var lon = currentEvent.Lon;
  var ContentCluster = currentEvent.ContentCluster;
  var AdditionalInfo = currentEvent.AdditionalInfo;

  var jsonEvent = JSON.stringify(currentEvent);

  var jsonEvent = currentEvent.toJsonData();

  var ccString = JSON.stringify(currentEvent.ContentCluster);

  // send the post request to the server
  sendPostRequest("SaveEvent/", {
    'EventName': ev_name,
    'EventCategory': ev_cat,
    'EventSubcategory': ev_subcat,
    'SocialMedia': currSocialMedia,
    'Comment' : comment,
    "Lat" : currentEvent.Lat,
    "Lon" : currentEvent.Lon,
    "StartDate" : currentEvent.StartDate,
    "EndDate" : currentEvent.EndDate,
	"HowOften" : ev_how_often,
    "AdditionalInfo" : JSON.stringify(currentEvent.AdditionalInfo),
    "ContentCluster" : ccString,
  }, saveSuccess,
    saveFailed
  );
}

/**
 * Success handler for the saving in database
 * 
 * @param response The response of the http post request
 * 
 */

function saveSuccess(response) {
  alert(response["message"]);
}

/**
 * Error handler for the saving in database
 * 
 * @param response The response of the http post request
 * 
 */
function saveFailed(response) {
  alert("Error!!");
}

/**
 * sets the currently displayed events
 * 
 */
function setCurrentEvents(events, type, socialMedia) {
  currentEvents = events;
  currSocialMedia = socialMedia;
  if (currentEvents) {
    if (type == "New_Events") {
      displayEvents(currentEvents, currSocialMedia, onMarkerClicked);
    }
    else {
      displayEvents(currentEvents, currSocialMedia, onDBMarkerClicked);
    }
  }
}

/**
 * sets the input from the django input fields
 * 
 */
function setDjangoInput(events, query, social_media, type) {
  currSocialMedia = social_media;
  currentEvents = events;
  currentType = type;
}



/**
 *  Set the initial values for the new events page
 */
function setInitialValueForNewEvents(name, startdate, enddate) {
  document.getElementById("id_event_name").value = name;
  document.getElementById("id_start_date").value = startdate;
  document.getElementById("id_end_date").value = enddate;
}

/**
 * initialize the leaflet map
 * 
 */
function map_init(map, options) {
  eventmap = map;

  // add the category legend to the map
  //addLegendToMap();
  
  createLegend();

  // add the layers for the clusters and the heatmap to the leaftlet map
  var layers = {
    "Heatmap" : heatmapLayer,
    "Cluster" : markerCluster
  };
  L.control.layers({}, layers, {position: 'topleft'}).addTo(eventmap);
  eventmap.addLayer(markerCluster);

  //uncomment this to show maximum bounding box
  //displayMaximumBoundingBox();

  /*if (currentType == "New_Events") {
		eventmap.removeControl(Legend);
  } */
  
  // if events are given then display them on the map
  if (currentEvents) {
    if (currentType == "New_Events") {
      displayEvents(currentEvents, currSocialMedia, onMarkerClicked);
	  eventmap.removeControl(Legend);
    }
    else {
      displayEvents(currentEvents, currSocialMedia, onDBMarkerClicked);
    }
  }
}

/**
 *  display the events on the map
 * 
 */
function displayEvents(events, social_media, markerClickedCallback) {
  currentMarkerIndex = 0;
  
  currentMarkers = [];
  eventsMarkerLists = [];

  markerCluster.clearLayers();

  //reset legend if not database
  /*if (social_media !== "Database") {
    document.getElementById("legend").innerHTML = "";
  } */

  if (events === "")
    return;
  var jsonString = decodeHtml(events);
  var jsonData;

  heatmapLayer.setLatLngs([]);

  try {
    jsonData = JSON.parse(jsonString);
    var arrayLength = jsonData.length;
    var eventList = new Array();

    for (var index = 0; index < arrayLength; index++) {
      var markerList = new Array();

      var eventCounter = 0;


      // get location info of current event
      var curLocation = jsonData[index]['location'];

      for (var eventCounter = 0; eventCounter < jsonData[index]["events"].length; ++eventCounter) {
        var currentEvent = jsonData[index]["events"][eventCounter];

        //special handling for facebook
        if (social_media === "Facebook") {
          currentEvent.AdditionalInfo["category"] = convertFbCategoryToDBCategory(currentEvent.AdditionalInfo["category"]);
        }

        var marker_icon;

        if( social_media === "Database") {
          marker_icon = getCategoryIcon(currentEvent.AdditionalInfo["category"], social_media,currentEvent.AdditionalInfo["social_media"] );

        }
        else {
          marker_icon = getCategoryIcon(currentEvent.AdditionalInfo["category"], social_media);

        }

        var amountOfEvents = jsonData[index]["events"].length
        var identifier = currentEvent.Identifier;

        var popup = generateMarkerPopup( identifier, currentEvent.StartTime, currentEvent.EndTime
      , currentEvent.AdditionalInfo["category"], currentEvent.AdditionalInfo["subcategory"]
      ,  currentEvent.AdditionalInfo["comments"], currentEvent.AdditionalInfo["howoften"], eventCounter, index, jsonData[index]["events"].length);

        var currentEventMarker = new EventMarker(
          identifier,
          currentEvent.StartTime,
          currentEvent.EndTime,
          currentEvent.Latitude,
          currentEvent.Longitude,
          currentEvent.ContentCluster,
          currentEvent.AdditionalInfo,
          popup,
          marker_icon
        )

        heatmapLayer.addLatLng(L.latLng(currentEvent.Latitude,currentEvent.Longitude));
        markerList.push(currentEventMarker);
      }


      var currentEvent = markerList[0];
      var marker = L.marker([currentEvent.Lat, currentEvent.Lon], { icon: marker_icon }); 
      marker.on('click', markerClickedCallback);
      marker.ContentCluster = currentEvent.ContentCluster;
      marker.ListID = index;
      marker.Title = identifier;
      marker.SocialMedia = social_media;
      marker.StartDate = currentEvent.StartTime;
      marker.EndDate = currentEvent.EndTime;
      marker.AdditionalInfo = currentEvent.AdditionalInfo;

      marker.bindPopup(generateMarkerPopup( currentEvent.Title, currentEvent.StartDate, currentEvent.EndDate
      , currentEvent.getCategory(), currentEvent.getSubcategory(), currentEvent.getComments(), 0, index, eventCounter));

      currentMarkers.push(marker);
      eventsMarkerLists.push(markerList);
      markerCluster.addLayer(marker);
    }
  } catch (e) {

    alert(e); //error in the above string(in this case,yes)!
  }
}

/**
 * click event handler for markers comming from social media
 * 
 */
function onMarkerClicked(e) {
  currentEvent = new Event(this.Title, this.ContentCluster);

  currentMarkerIndex = 0;
  currentEvent = new Event(this.Title, this.ContentCluster);
 
   var currentMarkerList = eventsMarkerLists[this.ListID];

   if( currentMarkerList[0].LocationText == "") {
     var latLngs = this.getLatLng(); 
     var locationString = getLocationInfo(latLngs.lat, latLngs.lng);

     //var locationInfo = getLocationInfo(curLocation.split(",")[0], curLocation.split(",")[1]);
      for( var counter = 0; counter < currentMarkerList.length; ++counter) {
        var curMarker = currentMarkerList[counter];
        curMarker.LocationText = locationString;
        curMarker.Popup = curMarker.Popup.replace("{locationText}", locationString);
    }
   }

  currentEvent = currentMarkerList[0];

  if( document.getElementById("id_ev_name")) {
   document.getElementById("id_ev_name").value = currentEvent.Title;
  }
  
  this.bindPopup( currentMarkerList[0].Popup );
  this.setIcon(currentMarkerList[0].Icon);

  $("#Additional_Info").empty();
  $('#show_save_form').show();

  //$('#additional_information').innerHTML = ;
  contentCluster = this.ContentCluster;
  if (currSocialMedia == "Twitter") {
    $.getScript('http://platform.twitter.com/widgets.js');
    for (var i = 0; i < contentCluster.length && i <= maxDisplayInfo; i++) {
      try {
        addTweetContent(contentCluster[i]);
      }
      catch (e) {
        alert(i);
      }
    }
    // reload twitter widgets for dynamically added content
    twttr.widgets.load();
  }
  else if (currSocialMedia == "Flickr") {
    for (var i = 0; i < contentCluster.length; i++) {
      addFlickrContent(contentCluster[i]);
    }
  }
}

/**
 *  click event handler for events comming from database events
 * 
 */
function onDBMarkerClicked(e) {
  currentMarkerIndex = 0;

   var currentMarkerList = eventsMarkerLists[this.ListID];

   if( currentMarkerList[0].LocationText == "") {
     var latLngs = this.getLatLng(); 
     var locationString = getLocationInfo(latLngs.lat, latLngs.lng);

     //var locationInfo = getLocationInfo(curLocation.split(",")[0], curLocation.split(",")[1]);
      for( var counter = 0; counter < currentMarkerList.length; ++counter) {
        var curMarker = currentMarkerList[counter];
        curMarker.LocationText = locationString;
        curMarker.Popup = curMarker.Popup.replace("{locationText}", locationString);
    }
   }
  this.bindPopup( currentMarkerList[0].Popup );
  this.setIcon(currentMarkerList[0].Icon);
  currentEvent = currentMarkerList[0];

  writeDBContentCluster(this.ContentCluster, this.AdditionalInfo);
}

/**
 * add content cluster ( like twitter tweets ) to webpage
 * 
 */
function writeDBContentCluster(contentCluster, additionalInfo) {
  $("#Additional_Info").empty();
  // var counter = 0;
  for (var counter = 0; counter < contentCluster.length && counter <= maxDisplayInfo; ++counter) {
    if (additionalInfo["HasTweets"] && counter <= additionalInfo["TweetsEndIndex"] && counter >= additionalInfo["TweetsStartIndex"]) {
      addTweetContent(contentCluster[counter]);
    }
    if (additionalInfo["HasFlickrPosts"] && counter <= additionalInfo["FlickrEndIndex"] && counter >= additionalInfo["FlickrStartIndex"]) {
      addFlickrContent(contentCluster[counter]);
    }
  }
  twttr.widgets.load();
}

/**
 * add tweet to webpage
 * 
 */
function addTweetContent(content) {
  $("#Additional_Info").append(
    "<li><blockquote class=\"twitter-tweet\" data-link-color=\"#55acee\" lang=\"en\"><p lang=\"en\">"
    + content.Title
    + "\"</p><a href=\""
    + content.URL + "\">Twitter</a></blockquote></li>");
}

/**
 * add flickr post to webpage
 * 
 */
function addFlickrContent(content) {

var flickrPostHtml = '<li>'
  flickrPostHtml += '<hr />';
  flickrPostHtml += '<div id="flickr_post">'
  flickrPostHtml += '<img src="https://s.yimg.com/pw/images/goodies/white-flickr.png"/>';
  flickrPostHtml += '<img src="https://s.yimg.com/pw/images/goodies/white-small-circle.png" align="right"/>';
  flickrPostHtml += '<a href="' + content.URL + '"><h5 style="font-color: #0063dc;"> ' + content.Title +  '</h5></a>'; 
  flickrPostHtml += '</div>';
  flickrPostHtml += '</li>';


  $("#Additional_Info").append(flickrPostHtml);
}

/**
 * get the bounds of the currently displayed map area
 * 
 */
function getMapBounds() {
  var toBig = false;

  // the maximum size of the bounding box
  var minimumSouth = 47.923046;
  var maximumNorth = 48.427553;
  var minimumWest = 11.199888;
  var maximumEast = 12.030220;



  var bounds = eventmap.getBounds();

  var south = bounds.getSouth();
  var north = bounds.getNorth();
  var west = bounds.getWest();
  var east = bounds.getEast();

  // check south
  if (south < minimumSouth) {
    south = minimumSouth;
    toBig = true;
  }

  if (north > maximumNorth) {
    north = maximumNorth;
    toBig = true;
  }

  if (west < minimumWest) {
    west = minimumWest;
    toBig = true;
  }

  if (east > maximumEast) {
    east = maximumEast;
    toBig = true;
  }

  if (toBig) {
    alert("Boundingbox will be restricted to munich!");
  }

  var southWest = L.latLng(south, west);
  var northEast = L.latLng(north, east);

  return L.latLngBounds(southWest, northEast);
}

/**
 * display maximum bunding box
 * 
 */
function displayMaximumBoundingBox() {
  //show maximum bounding box

  var minimumSouth = 47.923046;
  var maximumNorth = 48.427553;
  var minimumWest = 11.199888;
  var maximumEast = 12.030220;
  var bounds = [[minimumSouth, minimumWest], [maximumNorth, maximumEast]];
  // create an orange rectangle
  L.rectangle(bounds, { color: "#ff7800", weight: 1 }).addTo(eventmap);
}

/**
 * create the legend layer
 * 
 */
function createLegend() {
  Legend = new L.Control.Legend({
    position: 'bottomright',
    collapsed: true,
    controlButton: {
      title: "Legend"
    }
  });
  }

/**
 * add the legend layer to the map
 * 
 */
function addLegendToMap() {
	if (!Legend._map) {
	
		legend_div=document.createElement('div');
		legend_div.id = "legend";
		document.getElementById("legend_place").appendChild(legend_div);
	
		generateLegend();
	
	  eventmap.addControl(Legend);	
  
		$(".legend-container").append($("#legend"));
		$(".legend-toggle").append("<p style='text-align: center; font-weight: bold; font-size:1.5em; color:red;'>L</p>");
		//eventmap.addControl(Legend);
 }
 }
 
 /**
 * remove the legend layer from the map
 * 
 */
function removeLegendFromMap() {
	if (Legend._map) {
	eventmap.removeControl(Legend);
 }
}


/**
 * icon create function for the markerclusters
 * 
 */
function ownIconCreateFunction(cluster) {

 // 1-20,20-40, 40-60, 60-80, 80-100
  var childCount = cluster.getChildCount();
  var c = ' marker-cluster-';
  if (childCount < 20) {
    c += 'smaller-20';
  } else if (childCount <40) {
    c += 'smaller-40';
      } else if (childCount < 60) {
    c += 'smaller-60';
      } else if (childCount < 80) {
    c += 'smaller-80';
      } else if (childCount < 100) {
    c += 'smaller-100';
  } else {
    c += 'bigger-100';
  }

  return new L.DivIcon({ html: '<div><span>' + childCount + '</span></div>', className: 'marker-cluster' + c, iconSize: new L.Point(0,0) });
}


/**
 * generate the marker popup
 * 
 */
function generateMarkerPopup(name, startdate, enddate, category, subcategory, comments, howoften, eventCounter, index, amountOfEvents) {
  var popup = "";
 
  popup += '<h5 style="font-weight:bold"> Event Name: ' + name + '</h5>';
  popup += '<h6> From ' + convertDateString(startdate) + ' to ' + convertDateString(enddate) + '</h6>';
 // popup += '<h6> Location: ' + latitude + ', ' + longitude + '</h6>';
  popup += '<h6> Location: {locationText}</h6>';

  if( typeof comments !== "undefined" && comments !== "") {
    popup += '<h6> Comments: ' + comments + '</h6>';
  }
  
  if( typeof category !== "undefined" && category !== "" && category !== "Other") {
    popup += '<h6> Category: ' + capitalizeFirstLetter(category);

    if(typeof subcategory !== "undefined" && subcategory !== "") {
      popup += " > " + capitalizeFirstLetter(subcategory);
    }
    
    popup += '</h6>';
  }

    if( typeof howoften !== "undefined" && howoften !== "") {
      popup += '<h6> Howoften: ' + howoften + '</h6>';
    }

   

    if( amountOfEvents != 1) {
    popup += '<div class="popup_event_div">';
    popup += '<button type="button" id="markerlist-"' + index + ' onclick="previousClicked(' + index + ')" class="popup_button"><i class="fa fa-chevron-left aria-hidden="true"></i> Previous </button> ';
    popup += 'Event ' + (eventCounter + 1) + ' of ' + amountOfEvents;
    popup += ' <button type="button" id="markerlist-"' + index + ' onclick="nextClicked(' + index + ')" class="popup_button"> Next <i class="fa fa-chevron-right" aria-hidden="true"></i></button>';
    popup += '</div>';
 }
  return popup;

}

/**
 *  Clicked-Handler for next button in marker popup
 * 
 */
function nextClicked(id) {
  var currentMarkerList = eventsMarkerLists[id];
  currentMarkerIndex = (currentMarkerIndex + 1) % currentMarkerList.length;
  updateMarkerAtID(id, currentMarkerIndex);
}

/**
 * Clicked-Handler for previous button in marker popup
 * 
 */
function previousClicked(id) {
  var currentMarkerList = eventsMarkerLists[id];
  currentMarkerIndex = (currentMarkerIndex - 1) % currentMarkerList.length;
  if(currentMarkerIndex < 0) {
    currentMarkerIndex = currentMarkerList.length - 1;
  }
  updateMarkerAtID(id, currentMarkerIndex);
}

/**
 *    Update the marker at a given id
 * 
 */
function updateMarkerAtID(id, currentMarkerIndex) {
  var currentMarkerList = eventsMarkerLists[id];
  var nextEvent = currentMarkerList[currentMarkerIndex];

  currentMarkers[id]._popup.setContent(nextEvent.Popup);
  currentMarkers[id].setIcon(nextEvent.Icon);
  
  if( document.getElementById("id_ev_name")) {
   document.getElementById("id_ev_name").value = nextEvent.Title;
  }

  if( currSocialMedia == "Database") {
      currentEvent = nextEvent;
      writeDBContentCluster(nextEvent.ContentCluster, nextEvent.AdditionalInfo);
  }
}
