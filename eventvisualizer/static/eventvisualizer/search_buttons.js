/**
 * Search functions.

 * @author Astrid Berchtold <berchtoa@in.tum.de>
 * @author Katharina Bui <buik@in.tum.de>
 * @author Dominik Drexler <drexledo@in.tum.de>
 */

/**
 * Method for searching events in facebook
 * 
 * This method only calls the main search method with "Facebook" parameter
 */

function searchFacebook() {
	addLegendToMap();
	generateLegend();
    searchSocialMedia("Facebook");	
}

/**
 * Method for searching events in twitter
 * 
 * This method only calls the main search method with "Twitter" parameter
 */
function searchTwitter() {
    searchSocialMedia("Twitter");
};

/**
 * Method for searching events in Flickr
 * 
 * This method only calls the main search method with "Flickr" parameter
 */
function searchFlickr() {
    searchSocialMedia("Flickr");	
}

/**
 * Method for searching events in Xing
 * 
 * This method only calls the main search method with "Xing" parameter
 */
function searchXing() {
    searchSocialMedia("Xing");
}


/**
 * Method for searching events
 * 
 * This method gathers all data from the input fields and sends a get request to the server
 * 
 * @param socialMedia The type of social media where the events should be searched
 *         Possible values: Facebook, Twitter, Xing, Flickr, Database 
 */
function searchSocialMedia(socialMedia) {
	
//document.getElementById("MyElement").classList.remove('MyClass');
	document.getElementById("grayBackground").classList.add('disabledbutton');
	
	if ($('#loadingDiv') != "") {
        $('#loadingDiv').show();
    }
	
    currSocialMedia = socialMedia;
	
	if( currSocialMedia == 'Database' || currSocialMedia == 'Facebook')
	{
		addLegendToMap();
	}
	else {
		removeLegendFromMap();
	} 

    $("#Additional_Info").empty();

    var query = document.getElementById("id_event_name").value;
    var start_date = document.getElementById("id_start_date").value;
    var end_date = document.getElementById("id_end_date").value;
    
    if( socialMedia != "Database") {

    if( (typeof start_date == "undefined" || start_date == "") && (typeof end_date == "undefined" || end_date == "") ) {
        alert("The search interval will be set to the last two weeks!");
    }
    }


    var category = "";
    if(document.getElementById("id_category")) {
        category = document.getElementById("id_category").value;
    };

    if ($('#loading') != "") {
        $('#loading').show();
    }

    var bounds = getMapBounds();

    var maxLat = parseFloat(bounds.getNorth());
    var minLat = parseFloat(bounds.getSouth());
    var minLon = parseFloat(bounds.getWest());
    var maxLon = parseFloat(bounds.getEast());
	
	query = convertUmlaut(query);

    sendGetRequest("#", {
        'Query': query,
        'StartDate': start_date,
        'EndDate': end_date,
        'Category': category,
        'MinLat': minLat,
        'MaxLat': maxLat,
        'MinLon': minLon,
        'MaxLon': maxLon,
        'SocialMedia': socialMedia
    }, showEvents, errorHandler
    );
}

/**
 * Method for displaying the found events
 * 
 * This method calls the basic function which displays all events on the map
 * 
 * @param response The HTTP Get Request response from the server.
 *        If the request was successfull it contains all events as json objects.
 * 
 */
function showEvents(response) {

    if ($('#loading') != "") {
        $('#loading').hide();
    }
	
	if ($('#loadingDiv') != "") {
        $('#loadingDiv').hide();
    }
	
	document.getElementById("grayBackground").classList.remove('disabledbutton');

    if(response["error"] == 1) {
        alert(response["message"]);
        return;
    }
		

    //show database events
    if( response['social_media'] == "Database") {
        setCurrentEvents(response['content'], "Database_Events", response['social_media']);
    }
    else {
        setCurrentEvents(response['content'], "New_Events", response['social_media']);
    }
	
	

}

/**
 * Handler if http get request was not successfull
 * 
 * @param xhr The http response header
 * @param errmsg The error message
 * @param err The error
 * 
 */
function errorHandler(xhr, errmsg, err) {
    alert("Unknown error for request");

    if ($('#loading') != "") {
        $('#loading').hide();
    }

}


/**
 * Clear the form
 */
function clearForm() {
    document.getElementById("id_event_name").value = "";
    document.getElementById("id_start_date").value = "";
    document.getElementById("id_end_date").value = "";
}
