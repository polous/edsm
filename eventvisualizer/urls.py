from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.search_events, name='search_events'),
    url(r'NewEvents/$', views.new_events, name='new_events'),
    url(r'NewEvents/SaveEvent/$', views.save_event, name='save_events'),
    url(r'About/$', views.about, name='about'), 
]
