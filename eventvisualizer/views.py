# -*- encoding: utf-8-*-
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.shortcuts import redirect
from django.http import HttpResponse
from django.http import JsonResponse
from django.template import loader
from django.template import RequestContext

from django.http import HttpResponseRedirect

from .forms import EventSearchForm
from .forms import EventSaveForm
from datetime import date, timedelta, datetime
import json

from inc.helper import GeoBoundingBox

from Framework.EventDetection.TwitterEventDetection import TwitterEventDetection
from Framework.EventDetection.FlickrEventDetection import FlickrEventDetection
from Framework.EventDetection.InstagramEventDetection import InstagramEventDetection
from Framework.EventDetection.FacebookEventDetection import FacebookEventDetection

from Framework.EventProvider.DatabaseEventProvider import DatabaseEventProvider
from Framework.EventProvider.FacebookEventProvider import FacebookEventProvider
from Framework.EventProvider.XingEventProvider import XingEventProvider

date_format = ""

class EventEncoder(json.JSONEncoder):
    """
    JSONEncoder subclass that leverages an object's `__json__()` method,
    if available, to obtain its default JSON representation.

    """
    def default(self, obj):
        if hasattr(obj, '__json__'):
            return obj.__json__()
        return json.JSONEncoder.default(self, obj)
       

def index(request):
    """
    """
    template = loader.get_template('eventvisualizer/base.html')
    return HttpResponse(template.render(request))

def about(request):
    template = loader.get_template('eventvisualizer/about.html')
    return HttpResponse(template.render(request))

# search for events in the database
def search_events(request):
    """
    """

    if request.method == 'GET':
        formData = getFormData(request)
        if formData["social_media"] == "Database":
            inputValidation = checkDatabaseSearchInput( formData["event_name"], formData["start_date"],formData["end_date"],formData["category"])

            if inputValidation["error"] == 1:
                return JsonResponse(inputValidation)

            return showDatabaseEvents(request)
        
        if "eventid" in formData:
            return showDatabaseEvent(formData, request)

    form = EventSearchForm()
    template = loader.get_template('eventvisualizer/search_events.html')
    return HttpResponse(template.render({'form' : form}, request))


# store current event in database
def save_event(request):
    """
    """
 
    if not request.user.is_authenticated:
        context = {
            "error" : 1,
            "message" : "Please login first!"
         }
        return JsonResponse(context)

    if request.method == 'POST':
        requestData = request.POST        
        dbCon = DatabaseEventProvider()
        result = dbCon.storeEventToDatabase(requestData)
        return JsonResponse(result)

    form = EventSearchForm()
    save_event_form = EventSaveForm()
    template = loader.get_template('eventvisualizer/new_events.html')
    return HttpResponse(template.render({'form' : form, 'save_event_form' : save_event_form}, request))

# search for new events with the framework
def new_events(request):
    """
    """

    if request.method == 'GET':
        formData = getFormData(request)

        if 'source' in formData:
            form = EventSearchForm()
            form.event_name = formData["name"]
            form.start_date = formData["startdate"]
            form.end_date = formData["enddate"]
            save_event_form = EventSaveForm()
            template = loader.get_template('eventvisualizer/new_events.html')
            return HttpResponse(template.render({'form' : form, 'save_event_form' : save_event_form, 'name' : formData["name"], 'startdate' : formData["startdate"], 'enddate' : formData['enddate']}, request))
            

        if formData['social_media'] != "":
            validInput = checkSocialMediaSearchInput(formData['start_date'], formData['end_date'])

            if validInput["error"] == 1:
                return JsonResponse(validInput)

            if formData['social_media'] == 'Twitter':
                return showTwitterEvents(request)
            elif formData['social_media'] == 'Flickr':
                return showFlickrEvents(request)
            elif formData['social_media'] == 'Instagram':
                return showInstagramEvents(request)
            elif formData['social_media'] == 'Facebook':
                return showFacebookEvents(request)
            elif formData['social_media'] == 'Xing':
                return showXingEvents(request)
    
    form = EventSearchForm()
    save_event_form = EventSaveForm()
    template = loader.get_template('eventvisualizer/new_events.html')
    return HttpResponse(template.render({'form' : form, 'save_event_form' : save_event_form}, request))

def showDatabaseEvent(formData, request):
    """
    """

    form = EventSearchForm()
    template = loader.get_template('eventvisualizer/search_events.html')
    dbCon = DatabaseEventProvider()
    event = dbCon.getEvent(formData["eventid"])
    event = sortEventsByLocation(event)
    json_event = json.dumps(event, cls=EventEncoder)
    context = {
        "content": json_event,
        "social_media" : "Database",
        "form" : formData['form']
    }
    return HttpResponse(template.render(context, request))

# show database events
def showDatabaseEvents(request):
    """
    """
 
    formData = getFormData(request)
    dbCon = DatabaseEventProvider()
    events = dbCon.getEvents(formData['event_name'],formData['start_date'], formData['end_date'],formData['category'], formData['geoBoundingBox'])
    events = sortEventsByLocation(events)
    json_events = json.dumps(events, cls=EventEncoder)
    context = {
        "content": json_events,
        "social_media" : "Database"
    }
    return JsonResponse(context)

# show events from twitter
def showTwitterEvents(request):
    """
    """
 
    formData = getFormData(request)
    twitterED = TwitterEventDetection(formData['event_name'],  formData['start_date'], formData['end_date'], formData['geoBoundingBox'])
    return showEvents(request,twitterED, formData['form'], "Twitter")

#show events from flickr
def showFlickrEvents(request):
    """
    """
 
    formData = getFormData(request)
    flickrED = FlickrEventDetection(formData['start_date'], formData['end_date'], formData['geoBoundingBox'])
    return showEvents(request, flickrED, formData['form'], "Flickr")

# show events from instagram
def showInstagramEvents(request):
    """
    """

    formData = getFormData(request)
    instagramED = InstagramEventDetection(formData['start_date'], formData['end_date'], formData['geoBoundingBox'])
    return showEvents(request, instagramED, formData['form'], "Instagram")

# show events from facebook
def showFacebookEvents(request):
    """
    """
 
    formData = getFormData(request)
    fbProvider = FacebookEventProvider()
    events = fbProvider.getEvents(formData['event_name'],formData['start_date'], formData['end_date'], formData['geoBoundingBox'])
    return sendEvents(request, events, formData['form'], "Facebook")

    # show events from facebook
def showXingEvents(request):
    """
    """
 
    formData = getFormData(request)
    xingProvider = XingEventProvider()
    events = xingProvider.getEvents(formData['event_name'],formData['start_date'], formData['end_date'], formData['geoBoundingBox'])
    return sendEvents(request, events, formData['form'], "Xing")

# show the detected events
def showEvents(request, eventDetection, form, social_media):
    """
    """
 
    events = eventDetection.getContent()
    return sendEvents(request, events, form, social_media)


def sendEvents(request, events, form, social_media):
    """
    """
 
    events = sortEventsByLocation(events)
    json_events = json.dumps(events, cls=EventEncoder)
    template = loader.get_template('eventvisualizer/new_events.html')
    save_event_form = EventSaveForm()
    context = {
        "content": json_events,
        "social_media" : social_media,
    }
    return JsonResponse(context)

# get the form data from django input forms
def getFormData(request):
    """
    """
 
    data = { 'event_name':"", 'start_date':"", 'end_date' : "", 'form' : EventSearchForm(), 'social_media' : ""} #, "minLat" : "", "minLon" : "", "maxLat" : "", "maxLon" : ""}
    
    if request.method == 'GET':
        # create a form instance and populate it with data from the request:
        requestDict = request.GET.dict()
        if 'Query' in requestDict:
            if 'SocialMedia' in requestDict:
                if 'MinLat' in requestDict:
                    minLat = float(requestDict['MinLat'])
                    minLon = float(requestDict['MinLon'])
                    maxLat = float(requestDict['MaxLat'])
                    maxLon = float(requestDict['MaxLon'])

                    data['event_name'] = requestDict['Query']
                    data['start_date'] = requestDict['StartDate']
                    data['end_date'] = requestDict['EndDate']
                    data['social_media'] = requestDict['SocialMedia']
                    data['geoBoundingBox'] = GeoBoundingBox(minLat, minLon, maxLat, maxLon )
                    social_media = requestDict['SocialMedia']
                    if data['social_media'] != "Database":

                        start_date = None
                        end_date = None
                        if data['start_date'] == "":
                            if data['end_date'] == "":
                                end_date = datetime.now().date()
                                start_date = (datetime.now() - timedelta(days = 14)).date()
                                data['start_date'] = start_date
                                data['end_date'] = end_date

                            else:
                                end_date = datetime.strptime(data['end_date'], "%d/%m/%Y")
                                start_date = end_date  - timedelta(days = 14)
                                data['start_date'] = start_date.date()
                                data['end_date'] = end_date.date()
                        
                        else:
                            if data['end_date'] == "":
                                start_date = datetime.strptime(data['start_date'], "%d/%m/%Y")
                                end_date = start_date  + timedelta(days = 14)    
                                data['start_date'] = start_date.date()
                                data['end_date'] = end_date.date()
                            else:
				print "here"
                                start_date = datetime.strptime(data['start_date'], "%d/%m/%Y")
                                end_date = datetime.strptime(data['end_date'], "%d/%m/%Y") 
                                data['start_date'] = start_date.date()
                                data['end_date'] = end_date.date()


                    if 'Category' in requestDict:
                        data['category'] = requestDict['Category']
        else:
            if "eventid" in requestDict:
                data['eventid'] = requestDict['eventid']
            
            else:
                if "name" in requestDict or "startdate" in requestDict or "enddate" in requestDict:
                    data['name'] = ""
                    data['startdate'] = ""
                    data['enddate'] = ""
                    data['source'] = "bibek"
            
                    if "name" in requestDict:
                        data['name'] = requestDict['name']
                    if "startdate" in requestDict:
                        if data['startdate'] != "undefined":
                            data['startdate'] = requestDict['startdate']
                    if "enddate" in requestDict:
                        if requestDict['enddate'] != "undefined":
                            data['enddate'] = requestDict['enddate']
                    
    return data

# sort the events by their location
# Every event from the same location will be stored together
def sortEventsByLocation(events):
    """
    """
 
    orderedEvents = []
    for event in events:
        currentLocation = str(event.Location["center"]["latitude"]) +","+str(event.Location["center"]["longitude"])
        index = getIndexOfLocation(orderedEvents, currentLocation)
        if index == -1:
            orderedEvents.append( { "location" : currentLocation, "events" : [] } )
            index = len(orderedEvents) - 1
       
        addEventToList(event, orderedEvents[index]["events"])

    return orderedEvents

# Add event to the list
def addEventToList( newEvent, eventsList ):
    """
    """
 
    newEventStartDate = newEvent.Time["start"]
    newEventEndDate = newEvent.Time["end"]
    
    index = 0
    for event in eventsList:
        curStartDate = event.Time["start"]
        curEndDate = event.Time["end"]

        if curStartDate < newEventStartDate:
            index += 1
            continue
        elif curStartDate == newEventStartDate:
            if curEndDate > newEventEndDate:
                index += 1
                continue
            else:
                break
        break
    
    eventsList.insert(index, newEvent)

def getIndexOfLocation(eventList, location):
    """
    """
 
    index = 0
    for eventLocation in eventList:
        if eventLocation["location"] == location:
            return index
        index = index + 1
    return -1

# check if location is in the dictionary
def locationInDictionary(dictionary, location):
    for loc in dictionary:
        if location == loc:
            return True
    return False

# check if the start and enddate are correct
def checkSocialMediaSearchInput( start_date, end_date):
    """
    """
 
    inputValid = {"error": 0, "message" : ""}
    
    if start_date == "" and end_date == "":
        end_date = datetime.now().date()
        start_date = (datetime.now() - timedelta(days = 14)).date()
    
    elif start_date != "" and end_date != "":
        if isinstance(start_date, basestring):
            start_date = datetime.strptime(start_date, "%d/%m/%Y")
        
        if isinstance(end_date, basestring):
            end_date = datetime.strptime(end_date, "%d/%m/%Y")

        if start_date > end_date:
            inputValid["error"] = 1
            inputValid["message"] = "Startdate must be earlier than enddate!"

        else:
            dt = end_date - start_date
            if dt.days > 14:
                inputValid["error"] = 1
                inputValid["message"] = "Maximum timespan is 14 days for real time search!"

    return inputValid

# check if all input for database search is correct
def checkDatabaseSearchInput( event_name, start_date, end_date, category):
    """
    """
 
    inputValid = {"error": 0, "message" : ""}

    if start_date != "" and end_date != "":
        start_date_conv = datetime.strptime(start_date, "%d/%m/%Y")
        end_date_conv = datetime.strptime(end_date, "%d/%m/%Y")

        if start_date_conv > end_date_conv:
            inputValid["error"] = 1
            inputValid["message"] = "Startdate must be earlier than enddate!"
    
    return inputValid


