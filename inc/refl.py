import sys
from exceptions import Exception
import inspect

"""
	PROBLEM: 
		- currently doesn't work when arguments are passed as keyword arguments => need to use inspect module 
"""

class parameterTypeException(Exception):
	pass

class paramType(object):
	def __init__(self, ptype):
		self.type = ptype
		self._parent = None
		self._f = None
		self._pos = 0

	def __call__(self, f):
		if f.__name__ == "___refl_wrapped_f":
			self._parent = f.paramTypeDecorator
			self._f = self._parent._f
			self._parent._adjustPos()
		else:
			self._f = f

		def ___refl_wrapped_f(*args, **kwargs):
			print inspect.getcallargs(f, *args, **kwargs)
			if self._pos < len(args):
				if not(isinstance(args[self._pos], self.type)):
					raise parameterTypeException("parameter #%s (of function '%s') not of type '%s' (is '%s')" % (self._pos, paramType._getFuncQualName(self._f), paramType._getClassQualName(self.type), paramType._getObjQualName(args[self._pos])))
			f(*args, **kwargs)
		___refl_wrapped_f.paramTypeDecorator = self

		return ___refl_wrapped_f

	def _adjustPos(self):
		self._pos += 1
		if not(self._parent is None):
			self._parent._adjustPos()

	@staticmethod
	def _getFuncQualName(f):
		return f.__module__ + "." + f.__name__
	@staticmethod
	def _getObjQualName(o):
		return o.__class__.__module__ + "." + o.__class__.__name__
	@staticmethod
	def _getClassQualName(c):
		return c.__module__ + "." + c.__name__

"""
HOW TO USE:
class c:
	pass

@refl.paramType(c)
@refl.paramType(str)
@refl.paramType(str)
def f(a, b, d):
	print a


@refl.paramType(str)
def f2(a):
    print a

f(c(), "a", "a")
f2(2)
"""